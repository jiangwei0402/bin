#!/bin/bash - 
#===============================================================================
#
#          FILE: g.sh
# 
#         USAGE: ./g.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (LIGUOFENG), 
#  ORGANIZATION: 
#       CREATED: 2013年08月26日 14:45
#      REVISION:  ---
#===============================================================================

if [ $# == 0 ] ; then 
	echo "USAGE: $0 String"
	echo " e.g.: $0 String"
	exit 1;
fi

if [ -z $2 ]
then
	sudo find -not -path "./.git/*" \
		-not -path "./.svn/*" \
		-not -path "./out/*" \
		-not -path "./.repo/*" \
		-type f -print0 \
	| sudo xargs -0 -n1 -P8 grep "$1" -nrH \
	--exclude=cscope* --exclude=tags --color=auto 

else
	sudo find -not -path "./.git/*" \
		-not -path "./.svn/*" \
		-not -path "./out/*" \
		-not -path "./.repo/*" \
		-type f -print0 \
	| sudo xargs -0 -n1 -P8 grep "$1" -Hnr $2 \
	--exclude=cscope* --exclude=tags --color=auto 
fi
