#!/usr/bin/python
# coding: UTF-8
#----------------------------------------------------------------------------
# COPYRIGHT(C) FUJITSU CONNECTED TECHNOLOGIES LIMITED 2016
#----------------------------------------------------------------------------
# FCNT LIMITED:2016-06-01 H16200679 add start

import re
import os
import shutil
import sys

version_val=0

if (len(sys.argv) != 2) :
    print 'Usage: $ python %s <user/debug>' % sys.argv[0]
    quit()

pattern = "ro.build.display.id="
if (sys.argv[1] == "user") :
    pattern2 = pattern + "[a-zA-Z0-9_ ]+[^e] *\n$"
elif (sys.argv[1] == "debug") :
    pattern2 = pattern + "[a-zA-Z0-9_ ]+e *\n$"
else :
    print 'Usage: $ python %s <user/debug>' % sys.argv[0]
    quit()

#-------------------------------------------
# "#"が記述されている行は削除し、"ro.build.display.id="がある行だけを取り出す
#-------------------------------------------
try :
    fr = open('build.prop', 'r')
    fw = open('version_wk01.a120', 'w')
    while 1:
        s = fr.readline()
        if s == "":
            break
        if re.search("#", s) == None :
            if re.search(pattern2, s) != None :
                find = s.lstrip(pattern)
                fw.write(s)

    fr.close
    fw.close
except:
    print "version_error_1"
    fr.close
    fw.close

#-------------------------------------------
# "ro.build.display.id="→"ROM_VERSION="
#-------------------------------------------
fr = open('version_wk01.a120', 'r')
fw = open('version_wk02.a120', 'w')
s = fr.readline()
p1 = re.compile(pattern)
s = p1.sub("ROM_VERSION=", s)
fw.write(s)
fw.close
fr.close
version_val = s
#print version_val
#-------------------------------------------
# bjt.iniファイル読み出し
#-------------------------------------------
fr = open('bjt.ini', 'r')
s = fr.read()
fr.close
#print s
#-------------------------------------------
# "ROM_VERSION="→"ROM_VERSION=XXXXXX"
#-------------------------------------------
fw = open('bjt.ini', 'w')
p1 = re.compile("ROM_VERSION=")
s = p1.sub(version_val, s)
fw.write(s)

# FCNT LIMITED:2016-06-01 H16200679 add end
