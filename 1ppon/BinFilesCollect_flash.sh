#!/bin/bash

rom_type="32G"

if [ -n "$1" ]; then
    if [ "$1" = "32G" -o "$1" = "64G" ];then
        rom_type=$1
    elif [ -n "$2" ]; then
        if [ "$2" = "32G" -o "$2" = "64G" ];then
            rom_type=$2
        fi
    fi
fi

if test "$1" = "-cut" -o "$2" = "-cut"; then
  python -B ./_BinFilesCollect_flash.py "user" $rom_type "-cut"
else
  python -B ./_BinFilesCollect_flash.py "user" $rom_type
fi
