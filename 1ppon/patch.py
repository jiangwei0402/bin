#!/usr/bin/python
# coding: UTF-8
#----------------------------------------------------------------------------
# COPYRIGHT(C) FUJITSU CONNECTED TECHNOLOGIES LIMITED 2016
#----------------------------------------------------------------------------
# FCNT LIMITED:2016-06-01 H16200679 add start
import re
import os

MHDR = """;//*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
;////////Main0 Patch
;//*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
[Main0PatchInfo]
Main0PatchList=MPatch1QQQMPatch2QQQMPatch3QQQMPatch4QQQMPatch5QQQMPatch6\n
"""
BHDR = """;//*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
;////////Backup0 Patch
;//*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
[Backup0PatchInfo]
Backup0PatchList=BPatch1QQQBPatch2QQQBPatch3QQQBPatch4QQQBPatch5QQQBPatch6QQQBPatch7\n
"""
Mstr01 = """[MPatch1]
RegionName=MPatch1
"""
Mstr02 = """[MPatch2]
RegionName=MPatch2
"""
Mstr03 = """[MPatch3]
RegionName=MPatch3
"""
Mstr04 = """[MPatch4]
RegionName=MPatch4
"""
Mstr05 = """[MPatch5]
RegionName=MPatch5
"""
Mstr06 = """[MPatch6]
RegionName=MPatch6
"""

Bstr01 = """[BPatch1]
RegionName=BPatch1
"""
Bstr02 = """[BPatch2]
RegionName=BPatch2
"""
Bstr03 = """[BPatch3]
RegionName=BPatch3
"""
Bstr04 = """[BPatch4]
RegionName=BPatch4
"""
Bstr05 = """[BPatch5]
RegionName=BPatch5
"""
Bstr06 = """[BPatch6]
RegionName=BPatch6
"""
Bstr07 = """[BPatch7]
RegionName=BPatch7
"""

Mstr01flag = 0
Mstr02flag = 0
Mstr03flag = 0
Mstr04flag = 0
Mstr05flag = 0
Mstr06flag = 0

Bstr01flag = 0
Bstr02flag = 0
Bstr03flag = 0
Bstr04flag = 0
Bstr05flag = 0
Bstr06flag = 0
Bstr07flag = 0

#-------------------------------------------
# gpt_main0.bin
#-------------------------------------------
try :
    fr = open('patch0.xml', 'r')
    fw = open('patch_wk01.a120', 'w')
    while 1:
        s = fr.readline()
        if s == "":
            break
        if re.search("gpt_main0.bin", s) != None :
            fw.write(s),

    fr.close
    fw.close
except:
    print "patch_error_1"
    fr.close
    fw.close
#-------------------------------------------
# gpt_backup0.bin
#-------------------------------------------
try :
    fr = open('patch0.xml', 'r')
    fw = open('patch_wk01.a120', 'a+')
    while 1:
        s = fr.readline()
        if s == "":
            break
        if re.search("gpt_backup0.bin", s) != None :
            fw.write(s),

    fr.close
    fw.close
except:
    print "patch_error_2"
    fr.close
    fw.close
#-------------------------------------------
# " "
#-------------------------------------------
fr = open('patch_wk01.a120', 'r')
fw = open('patch_wk02.a120', 'w')
s = fr.read()
p1 = re.compile(" ")
s = p1.sub("\n", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "byte_offset"
#-------------------------------------------
try :
    fr = open('patch_wk02.a120', 'r')
    fw = open('patch_wk03.a120', 'w')
    while 1:
        s = fr.readline()
        if s == "":
            break
        #print s,
        # -------------------------------------
        # byte_offset
        # -------------------------------------
        if re.search("byte_offset", s) != None :
            if Mstr01flag == 0 :
                fw.write(MHDR)
                fw.write(Mstr01)
                Mstr01flag = 1
            else :
                if Mstr02flag == 0 :
                    fw.write(Mstr02)
                    Mstr02flag = 1
                else :
                    if Mstr03flag == 0 :
                        fw.write(Mstr03)
                        Mstr03flag = 1
                    else :
                        if Mstr04flag == 0 :
                            fw.write(Mstr04)
                            Mstr04flag = 1
                        else :
                            if Mstr05flag == 0 :
                                fw.write(Mstr05)
                                Mstr05flag = 1
                            else :
                                if Mstr06flag == 0 :
                                    fw.write(Mstr06)
                                    Mstr06flag = 1
                                else :
                                    if Bstr01flag == 0 :
                                        fw.write(BHDR)
                                        fw.write(Bstr01)
                                        Bstr01flag = 1
                                    else :
                                        if Bstr02flag == 0 :
                                            fw.write(Bstr02)
                                            Bstr02flag = 1
                                        else :
                                            if Bstr03flag == 0 :
                                                fw.write(Bstr03)
                                                Bstr03flag = 1
                                            else :
                                                if Bstr04flag == 0 :
                                                    fw.write(Bstr04)
                                                    Bstr04flag = 1
                                                else :
                                                    if Bstr05flag == 0 :
                                                        fw.write(Bstr05)
                                                        Bstr05flag = 1
                                                    else :
                                                        if Bstr06flag == 0 :
                                                            fw.write(Bstr06)
                                                            Bstr06flag = 1
                                                        else :
                                                            if Bstr07flag == 0 :
                                                                fw.write(Bstr07)
                                                                Bstr07flag = 1

            fw.write(s),
        else:
            # -------------------------------------
            # filename
            # -------------------------------------
            if re.search("filename", s) != None :
                fw.write(s),
            else:
                # -------------------------------------
                # physical_partition_number
                # -------------------------------------
                if re.search("physical_partition_number", s) != None :
                    fw.write(s),
                else:
                    # -------------------------------------
                    # size_in_bytes
                    # -------------------------------------
                    if re.search("size_in_bytes", s) != None :
                        fw.write(s),
                    else:
                        # -------------------------------------
                        # start_sector
                        # -------------------------------------
                        if re.search("start_sector", s) != None :
                            fw.write(s),
                        else:
                            # -------------------------------------
                            # CRC32
                            # -------------------------------------
                            if re.search("CRC32", s) != None :
                                fw.write(s),
                                fw.write("\n"),
                            else:
                                # -------------------------------------
                                # NUM_DISK_SECTORS
                                # -------------------------------------
                                if re.search("NUM_DISK_SECTORS", s) != None :
                                    fw.write(s),
                                    fw.write("CRC_calc=0\nCRC_start_sector=0\nCRC_read_byte=0\n\n"),
                                else:
                                    # -------------------------------------
                                    # value="0"
                                    # -------------------------------------
                                    if re.search("value=\"0", s) != None :
                                        fw.write(s),
                                        fw.write("CRC_calc=0\nCRC_start_sector=0\nCRC_read_byte=0\n\n"),

    fr.close
    fw.close
except:
    print "patch_error_3"
    fr.close
    fw.close
#-------------------------------------------
# "value="CRC32"
#-------------------------------------------
fr = open('patch_wk03.a120', 'r')
fw = open('patch_wk04.a120', 'w')
s = fr.read()
p1 = re.compile("value=\"CRC32")
s = p1.sub("Value=0\nCRC_calc=1\n", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "("
#-------------------------------------------
fr = open('patch_wk04.a120', 'r')
fw = open('patch_wk05.a120', 'w')
s = fr.read()
p1 = re.compile("\(")
s = p1.sub("CRC_start_sector=", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# ","
#-------------------------------------------
fr = open('patch_wk05.a120', 'r')
fw = open('patch_wk06.a120', 'w')
s = fr.read()
p1 = re.compile("\,")
s = p1.sub("\nCRC_read_byte=", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# ")"
#-------------------------------------------
fr = open('patch_wk06.a120', 'r')
fw = open('patch_wk07.a120', 'w')
s = fr.read()
p1 = re.compile("\)")
s = p1.sub("", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# """
#-------------------------------------------
fr = open('patch_wk07.a120', 'r')
fw = open('patch_wk08.a120', 'w')
s = fr.read()
p1 = re.compile("\"")
s = p1.sub("", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "value=0"
#-------------------------------------------
fr = open('patch_wk08.a120', 'r')
fw = open('patch_wk09.a120', 'w')
s = fr.read()
p1 = re.compile("value=0")
s = p1.sub("Value=0", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# """
#-------------------------------------------
fr = open('patch_wk09.a120', 'r')
fw = open('patch_wk10.a120', 'w')
s = fr.read()
p1 = re.compile("\"")
s = p1.sub("", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "."
#-------------------------------------------
fr = open('patch_wk10.a120', 'r')
fw = open('patch_wk11.a120', 'w')
s = fr.read()
p1 = re.compile("\.")
s = p1.sub("", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "gpt_main0bin"
#-------------------------------------------
fr = open('patch_wk11.a120', 'r')
fw = open('patch_wk12.a120', 'w')
s = fr.read()
p1 = re.compile("gpt_main0bin")
s = p1.sub("gpt_main0.bin", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "gpt_backup0bin"
#-------------------------------------------
fr = open('patch_wk12.a120', 'r')
fw = open('patch_wk13.a120', 'w')
s = fr.read()
p1 = re.compile("gpt_backup0bin")
s = p1.sub("gpt_backup0.bin", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "value=NUM_DISK_SECTORS-"
#-------------------------------------------
fr = open('patch_wk13.a120', 'r')
fw = open('patch_wk14.a120', 'w')
s = fr.read()
p1 = re.compile("value=NUM_DISK_SECTORS-")
s = p1.sub("Value=", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "byte_offset"
#-------------------------------------------
fr = open('patch_wk14.a120', 'r')
fw = open('patch_wk15.a120', 'w')
s = fr.read()
p1 = re.compile("byte_offset")
s = p1.sub("Byte_offset", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "filename"
#-------------------------------------------
fr = open('patch_wk15.a120', 'r')
fw = open('patch_wk16.a120', 'w')
s = fr.read()
p1 = re.compile("filename")
s = p1.sub("Filename", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "physical_partition_number"
#-------------------------------------------
fr = open('patch_wk16.a120', 'r')
fw = open('patch_wk17.a120', 'w')
s = fr.read()
p1 = re.compile("physical_partition_number")
s = p1.sub("Physical_partition_number", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "size_in_bytes"
#-------------------------------------------
fr = open('patch_wk17.a120', 'r')
fw = open('patch_wk18.a120', 'w')
s = fr.read()
p1 = re.compile("size_in_bytes")
s = p1.sub("Size_in_bytes", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "start_sector"
#-------------------------------------------
fr = open('patch_wk18.a120', 'r')
fw = open('patch_wk19.a120', 'w')
s = fr.read()
p1 = re.compile("start_sector")
s = p1.sub("Start_sector", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "QQQ"
#-------------------------------------------
fr = open('patch_wk19.a120', 'r')
fw = open('patch_wk20.a120', 'w')
s = fr.read()
p1 = re.compile("QQQ")
s = p1.sub(",", s)
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "CRC_Start_sector"
#-------------------------------------------
fr = open('patch_wk20.a120', 'r')
fw = open('bjt.ini', 'a+')
s = fr.read()
p1 = re.compile("CRC_Start_sector")
s = p1.sub("CRC_start_sector", s)
fr.close
fw.write(s),
fw.close
#-------------------------------------------
# "patch_wk**.a120 dell"
#-------------------------------------------
os.remove(r'./patch_wk01.a120')
os.remove(r'./patch_wk02.a120')
os.remove(r'./patch_wk03.a120')
os.remove(r'./patch_wk04.a120')
os.remove(r'./patch_wk05.a120')
os.remove(r'./patch_wk06.a120')
os.remove(r'./patch_wk07.a120')
os.remove(r'./patch_wk08.a120')
os.remove(r'./patch_wk09.a120')
os.remove(r'./patch_wk10.a120')
os.remove(r'./patch_wk11.a120')
os.remove(r'./patch_wk12.a120')
os.remove(r'./patch_wk13.a120')
os.remove(r'./patch_wk14.a120')
os.remove(r'./patch_wk15.a120')
os.remove(r'./patch_wk16.a120')
os.remove(r'./patch_wk17.a120')
os.remove(r'./patch_wk18.a120')
os.remove(r'./patch_wk19.a120')
#os.remove(r'./patch_wk20.a120')

# FCNT LIMITED:2016-06-01 H16200679 add end
