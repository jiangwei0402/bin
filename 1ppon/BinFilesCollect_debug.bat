@echo off
@setlocal ENABLEEXTENSIONS

if "%1" == "-cut" (
    if "%2" == "" (
        call :doing 32G -cut
    ) else (
        call :doing %2 -cut
    )
) else if "%2" == "-cut" (
    call :doing %1 -cut
) else if "%1" == "" (
    call :doing 32G
) else (
    call :doing %1
)
goto:EOF

:doing

if "%1" == "16G" (
  set ROMTYPE=16G
) else if "%1" == "32G" (
  set ROMTYPE=32G
) else (
  set ROMTYPE=32G
)

if "%2" == "-cut" (
  _BinFilesCollect.py "debug" %ROMTYPE% "-cut"
) else (
  _BinFilesCollect.py "debug" %ROMTYPE%
)

pause
