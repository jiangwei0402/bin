#!/bin/bash

rom_type="32G"

if [ -n "$1" ]; then
    if [ "$1" = "16G" -o "$1" = "32G" ];then
        rom_type=$1
    elif [ -n "$2" ]; then
        if [ "$2" = "16G" -o "$2" = "32G" ];then
            rom_type=$2
        fi
    fi
fi

if test "$1" = "-cut" -o "$2" = "-cut"; then
  python -B ./_BinFilesCollect.py "debug" $rom_type "-cut"
else
  python -B ./_BinFilesCollect.py "debug" $rom_type
fi
