#!/usr/bin/python
# coding: UTF-8

import os
import fnmatch
import glob
import sys
import shutil
import glob
import platform

import path_data

def print_log(str):
    if platform.system() == 'Windows':
        print str.encode('shift-jis')
    else:
        print str.encode('utf_8')

def file_copy(src, dst):
    src_files = glob.glob(src)
    if os.path.exists(src):
       print_log(u"把" + src + u"复制到" + dst + u"目录下")
    elif '*.' in src:
       src2 = src.replace('*','_1')
       if os.path.exists(src2):
           print_log(u"把" + src + u"复制到" + dst + u"目录下")
       elif '*.py' in src:
           print_log(u"把" + src + u"复制到" + dst + u"目录下")
       else:
           print_log(u"ERROR:"  + src + u"没有找到")
    else:
       print_log(u"ERROR:"  + src + u"没有找到")

    for file in src_files:
        file_cnv = file.replace('\\', '/')
        shutil.copy(file_cnv, dst)

def file_remove(src):
    src_files = glob.glob(src)

    for file in src_files:
        os.remove(file)

if __name__ == "__main__":

    param = sys.argv
    argc = len(param)

    if param[1] == 'debug':
      check_paths = glob.glob(path_data.check_path_debug)
    else:
      check_paths = glob.glob(path_data.check_path)

    count = 0

    for check_path in check_paths:
        check_path2 = check_path.replace('\\', '/')
        if os.path.isdir(check_path2):
            count = count + 1

    if count == 0:
        print_log(path_data.check_path + u"没有找到")
        sys.exit()
    elif count != 1:
        print_log(path_data.check_path + u"目录下多个文件夹")
        sys.exit()

    if os.path.isdir('32G') == False:
        os.mkdir('32G')

    if os.path.isdir('64G') == False:
        os.mkdir('64G')

    rom_type='32G'

    if param[2] == '32G':
        rom_type='32G'
    elif param[2] == '64G':
        rom_type='64G'

    num = 0
    while path_data.copy_path_table[num] != "":
        file_copy(path_data.copy_path_table[num], rom_type)
        num = num + 1

    num = 0
    while path_data.copy_dev_path_table[num] != "":
        file_copy(check_path2 + path_data.copy_dev_path_table[num], rom_type)
        num = num + 1

    if rom_type == '32G':
        file_copy(path_data.copy_ud_path_table[path_data.ud_id32G], rom_type)
    elif rom_type == '64G':
        file_copy(path_data.copy_ud_path_table[path_data.ud_id64G], rom_type)
    else:
        file_copy(path_data.copy_ud_path_table[path_data.ud_id], rom_type)

