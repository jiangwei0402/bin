#!/usr/bin/python
# coding: UTF-8
#----------------------------------------------------------------------------
# COPYRIGHT(C) FUJITSU CONNECTED TECHNOLOGIES LIMITED 2016
#----------------------------------------------------------------------------
# FCNT LIMITED:2016-06-01 H16200679 add start
# FCNT LIMITED:2016-07-04 H16200833 mod start

import re
import os
import shutil
import fnmatch
import sys
import partition_data

def _human_key(key): 
    parts = re.split('(\d*\.\d+|\d+)', key) 
    return tuple((e.swapcase() if partition_num % 2 == 0 else float(e)) 
            for partition_num, e in enumerate(parts)) 

argvs = sys.argv

HDR = """; // ===========================================================================
; // ===========================================================================
; //
; // when     who     what, where, why
; // -------- ------  ----------------------------------------------------------
; //
; // ===========================================================================

[Properties]"""
if '16G' == argvs[1]:
    HDR1 = """
OutFile=./f16golfz16_VnlRxxA.bin"""
elif '32G' == argvs[1]:
    HDR1 = """
OutFile=./f16golfz32_VnlRxxA.bin"""
HDR2 = """
RootDir=.
LargeBlockSize=00000200
DeviceSize=07740000

[FileHeader]
PartitionOffset=0003E000
BinaryImageOffset=00042000
Signature=Image file with header
Version=00000200
MainPatchOffset=00010000
BackPatchOffset=00020000
"""
if '16G' in argvs[1]:
    HDR3 = partition_data.HDR3_16G
elif '32G' in argvs[1]:
    HDR3 = partition_data.HDR3_32G
HDR4 = """

ROM_VERSION=
[PartitionInfo]
PartitionList="""

if '16G' in argvs[1]:
    NUM_DISK_SECTORS = partition_data.num_disk_sec_dec[0]
elif '32G' in argvs[1]:
    NUM_DISK_SECTORS = partition_data.num_disk_sec_dec[1]

PART_NAME_LEN_MAX = 4

#-------------------------------------------
# // part_name生成関数
#-------------------------------------------
def get_part_name(s2, head_name):
    num_name = s2[ s2.rfind("_")+1 : s2.rfind(".") ]
    
    if num_name.isdigit() == False :
        num_name = "1"
    
    if len(head_name) + len(num_name) > PART_NAME_LEN_MAX :
        delnum = (len(head_name) + len(num_name)) - PART_NAME_LEN_MAX
        head_name2 = head_name[:len(head_name)-delnum]
    else :
        head_name2 = head_name

    num_name = num_name.zfill(PART_NAME_LEN_MAX - len(head_name2))
    return head_name2 + num_name


#-------------------------------------------
# rawprogram_unsparse.xmlから"を削除
#-------------------------------------------
fr = open('rawprogram_unsparse.xml', 'r')
fw = open('_rawprogram_unsparse_copy.a120', 'w')
s = fr.read()
p1 = re.compile("\"")
s = p1.sub("", s)
fw.flush()
fw.write(s),
fw.close
fr.close
#-------------------------------------------
# "start_sector="が記述されている行を取り出す
# " "→"\n"
#-------------------------------------------
try :
    fr = open('_rawprogram_unsparse_copy.a120', 'r')
    fw = open('_effective_line.a120', 'w')
    p1 = re.compile(" ")
    while 1:
        s = fr.readline()
        if s == "":
            break
        if re.search("zeroout label=", s) == None :
            if re.search("start_sector=", s) != None :
                find = s.lstrip('start_sector=')
                s = p1.sub("\n", s)
                fw.write(s),

    fr.close
    fw.flush()
    fw.close
except:
    print "ERROR:It failed in taking out an effective line."
    fr.close
    fw.close
#-------------------------------------------
# "start_sector="が記述されている行を取り出す
# "start_sector="→""
# "NUM_DISK_SECTORS-33."の値を変換
#-------------------------------------------
try :
    fr = open('_effective_line.a120', 'r')
    fw = open('_start_sector_num.a120', 'w')
    p1 = re.compile("start_sector=")
    p2 = re.compile("NUM_DISK_SECTORS-33.")
    num_disk_sectors = int(NUM_DISK_SECTORS) - 33

    while 1:
        s = fr.readline()
        if s == "":
            break
        if re.search("start_sector=", s) != None :
            s = p1.sub("", s)
            s = p2.sub("%d"%(num_disk_sectors), s)

            fw.write(s),

    fr.close
    fw.flush()
    fw.close
except:
    print "ERROR:It failed in taking out start_sector."
    fr.close
    fw.close

#-------------------------------------------
# 総パーティション数を求める為ソートする
#-------------------------------------------
fr = open('_start_sector_num.a120', 'r')
s = fr.readlines()
s.sort(key=_human_key),

n = 0
partition_num = 0

for line in s:
    if line != n:
        n = line
        partition_num = partition_num + 1
    if s == "":
        break

fr.close

#-------------------------------------------
# "num_partition_sectors="が記述されている行を取り出す
# "num_partition_sectors="→""
#-------------------------------------------
try :
    fr = open('_effective_line.a120', 'r')
    fw = open('_num_partition_sectors_num.a120', 'w')
    p1 = re.compile("num_partition_sectors=")
    while 1:
        s = fr.readline()
        if s == "":
            break
        if re.search("num_partition_sectors=", s) != None :
            s = p1.sub("", s)
            fw.write(s),

    fr.close
    fw.flush()
    fw.close
except:
    print "ERROR:It failed in taking out num_partition_sectors."
    fr.close
    fw.close

#-------------------------------------------
# "label="が記述されている行を取り出す
# "label="→""
#-------------------------------------------
try :
    fr = open('_effective_line.a120', 'r')
    fw = open('_label_str.a120', 'w')
    p1 = re.compile("label=")
    while 1:
        s = fr.readline()
        if s == "":
            break
        if re.search("label=", s) != None :
            s = p1.sub("", s)
            fw.write(s),

    fr.close
    fw.flush()
    fw.close
except:
    print "ERROR:It failed in taking out label."
    fr.close
    fw.close
#-------------------------------------------
# "filename="が記述されている行を取り出す
# "filename="→""
#-------------------------------------------
try :
    fr = open('_effective_line.a120', 'r')
    fw = open('_filename_str.a120', 'w')
    p1 = re.compile("filename=")
    while 1:
        s = fr.readline()
        if s == "":
            break
        if re.search("filename=", s) != None :
            s = p1.sub("", s)
            fw.write(s),

    fr.close
    fw.flush()
    fw.close
except:
    print "ERROR:It failed in taking out filename."
    fr.close
    fw.close
#-------------------------------------------
# start_sector + filename + label
#-------------------------------------------
try :
    fr1 = open('_start_sector_num.a120', 'r')
    fr2 = open('_filename_str.a120', 'r')
    fr3 = open('_label_str.a120', 'r')
    fw  = open('_ssector_filename_label.a120', 'w')
    p1 = re.compile("\n")
    while 1:
        s = fr1.readline()
        t = fr2.readline()
        u = fr3.readline()
        if s == "":
            break
        s = p1.sub("", s)
        t = p1.sub("", t)
        u = p1.sub("", u)
        fw.write(s+",")
        fw.write(t+","+u+"\n")
    fr1.close
    fr2.close
    fr3.close
    fw.flush()
    fw.close
except:
    print "ERROR:It failed in the synthesis."
    fr1.close
    fr2.close
    fr3.close
    fw.close

#-------------------------------------------
# ソートする
#-------------------------------------------
fr = open('_ssector_filename_label.a120', 'r')
fw = open('_ssector_filename_label_sort.a120', 'w')

s = fr.readlines()
s.sort(key=_human_key),

fw.writelines(s)
fw.flush()
fw.close
fr.close

#-------------------------------------------
# ","→"\n"
#-------------------------------------------
fr = open('_ssector_filename_label_sort.a120', 'r')
fw = open('_ssector_filename_label_sort2.a120', 'w')
s = fr.read()
p1 = re.compile(",")
s = p1.sub("\n", s)
fw.write(s),
fw.flush()
fw.close
fr.close

# FCNT LIMITED:2016-09-01 H16200679-2 add start
#-------------------------------------------
# start_sector + num_partition + label
#-------------------------------------------
try :
    fr1 = open('_start_sector_num.a120', 'r')
    fr2 = open('_num_partition_sectors_num.a120', 'r')
    fr3 = open('_label_str.a120', 'r')
    start_num_label = []
    p1 = re.compile("\n")

    while 1:
        s = fr1.readline()
        t = fr2.readline()
        u = fr3.readline()
        if s == "":
            break
        s = p1.sub("", s)
        t = p1.sub("", t)
        u = p1.sub("", u)
        start_num_label.append(s+","+t+","+u+"\n")

    fr1.close
    fr2.close
    fr3.close
except:
    print "ERROR:It failed in the synthesis."
    fr1.close
    fr2.close
    fr3.close

#-------------------------------------------
# ソートする
#-------------------------------------------
start_num_label.sort(key=_human_key)

#-------------------------------------------
# ","→"\n"
#-------------------------------------------
fw = open('_ssector_num_label_sort.a120', 'w')

p1 = re.compile(",")

for line in start_num_label:
    line = p1.sub("\n", line)
    fw.write(line)

fw.flush()
fw.close
# FCNT LIMITED:2016-09-01 H16200679-2 add end
# FCNT LIMITED:2016-09-01 H16200679-2 mod start
#-------------------------------------------
# パーティションはみ出しチェック
#-------------------------------------------
try :
    fd = open('_ssector_num_label_sort.a120', 'r')

    start_s = fd.readline()
    size_s = fd.readline()
    label_s = fd.readline()
    
    while 1:
        next_addr = int(start_s) + int(size_s)

        start_s = fd.readline()
        size_s = fd.readline()
        if start_s == "":
            break
        if size_s == "":
            break
        if label_s != "grow\n":
            if next_addr > int(start_s):
                print "ERROR:partition size error!! " + label_s
                raise Exception

        label_s = fd.readline()

# FCNT LIMITED:2016-09-20 H16200679-3 add start
    if next_addr > int(NUM_DISK_SECTORS):
        print "ERROR:This exceeds the size of the device!!"
        raise Exception
# FCNT LIMITED:2016-09-20 H16200679-3 add end

    fd.close
except Exception:
    print "ERROR:partition check error"
    fd.close
    sys.exit()

# FCNT LIMITED:2016-09-01 H16200679-2 mod end

#-------------------------------------------
# [PartitionInfo]作成
#-------------------------------------------
j = 0
cont = 2048
part_name = cont*[0]
fr = open('_ssector_filename_label_sort2.a120', 'r')
fw = open('bjt.ini', 'w')
fw.write(HDR)
fw.write(HDR1)
fw.write(HDR2)
fw.write(HDR3)
fw.write(HDR4)

try :
    while 1:
        if j < partition_num:
            j = j+1
            s1 = fr.readline()
            s2 = fr.readline()
            s3 = fr.readline()
            num = 0
            while partition_data.part_name_table[num][0] != "":
                if s3 == partition_data.part_name_table[num][0] :
                    if partition_data.part_name_table[num][2] == 1:
                        if fnmatch.fnmatch(s2, partition_data.part_name_table[num][3]) :
                            part_name[j] = get_part_name(s2, partition_data.part_name_table[num][1])
                        else :
                            part_name[j] = partition_data.part_name_table[num][1]
                    else:
                        part_name[j] = partition_data.part_name_table[num][1]
                    break
                else :
                    num = num + 1

            if partition_data.part_name_table[num][0] == "":
                print "unknown partition " + s3
                part_name[j] = j

            if s3 == "BackupGPT\n" :
                fw.write("back"+"\n\n")
                break

            fw.write(part_name[j]+",")
        else :
            break
    fr.close
    fw.flush()
    fw.close
except:
    print "ERROR:It failed in the generation of bjt.ini."
    fr.close
    fw.close
#-------------------------------------------
# 各パーティションのデータ作成
#-------------------------------------------
mod1_val = 0
mod2_val = 0
mgap_val = 0
j = 0
s1 = 0
s2 = 0
s3 = 0
s4 = 0
s5 = 0
fr = open('_ssector_filename_label_sort2.a120', 'r')
fw = open('bjt.ini', 'a+')
try :
    s1 = fr.readline()
    while 1:
        s2 = fr.readline()
        s3 = fr.readline()
        s4 = fr.readline()
#       print s3
        if j < partition_num:
            j = j+1
            fw.write("["+part_name[j]+"]"+"\n")
            fw.write("InFile=")
            fw.write(s2)
            fw.write("RegionName=")
            fw.write(part_name[j]+"\n")
            fw.write("PartitionSectors=")
            if s3 == "modemst1\n":
                s5 = int(s4)- int(s1)
                fw.write("%d"%(s5))
                fw.write("\n\n\n")
                mod1_val = s5
                s1 = s4
            elif s3 == "modemst2\n":
                s5 = int(s4)- int(s1)
                #// sy01-mod2を計算してmod1の値と同じであればmgapは無し
                if s5 == mod1_val:
                    fw.write("%d"%(s5))
                    fw.write("\n\n\n")
                    s1 = s4
                else:
                    #//mgapがある
                    mod2_val = mod1_val
                    fw.write("%d"%(mod2_val))
                    fw.write("\n\n\n")
                    s1 = s4
                    #//mgapがあるのでmgap作成
                    fw.write("[mgap]\n")
                    fw.write("InFile=\n")
                    fw.write("RegionName=mgap\n")
                    fw.write("PartitionSectors=")
                    mgap_val = int(s5)- int(mod2_val)
                    fw.write("%d"%(mgap_val))
                    fw.write("\n\n\n")
                    j = j+1
            elif s2 == "gpt_backup0.bin\n" :
                fw.write("33"+"\n\n\n\n")
                break
            else :
                s5 = int(s4)- int(s1)
                fw.write("%d"%(s5))
                fw.write("\n\n\n")
                s1 = s4
        else :
            break
    fr.close
    fw.flush()
    fw.close
except:
    print "ERROR:It failed in the postscript of the partition data."
    fr.close
    fw.close

# FCNT LIMITED:2016-07-04 H16200833 mod end
# FCNT LIMITED:2016-06-01 H16200679 add end
