#!/usr/bin/python
# coding: UTF-8
#----------------------------------------------------------------------------
# COPYRIGHT(C) FUJITSU CONNECTED TECHNOLOGIES LIMITED 2016
#----------------------------------------------------------------------------
# FCNT LIMITED:2016-06-01 H16200679 add start

import os
import fnmatch

try :
	fr = open('bjt.ini', 'r')
	fw = open('bjt_wk.ini', 'w')
	while 1:
		s = fr.readline()
		if s == "":
			break
		elif fnmatch.fnmatch(s, "InFile=dic_*.img\n") :
			s = "InFile=\n"
		elif fnmatch.fnmatch(s, "InFile=cnt_*.img\n") :
			s = "InFile=\n"
		
		fw.write(s)

	fw.close
	fr.close

except:
	print "compact_error"
	fr.close
	fw.close

# FCNT LIMITED:2016-06-01 H16200679 add end
