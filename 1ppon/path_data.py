#!/usr/bin/python
# coding: UTF-8

import xml.dom.minidom

common_root = ''
linux_root = ''
build_root = ''
sparse_images_path = ''
check_path_debug = ''
check_path = ''
copy_org_file_table = []

copy_path_table = []
copy_dev_path_table = []
copy_ud_path_table = []

#leifen add start - 20171024
fsg_path = 'modem_proc/build/ms/'
#leifen add end - 20171024

ud_id = 0
ud_id32G = 1
ud_id64G = 2

def get_node(node, name):
    for node2 in node:
        if node2.nodeType == node2.ELEMENT_NODE:
            if node2.tagName == name:
                return node2
    return ""

def get_nodedata(node, name):
    name_node = get_node(node, name)
    for name_node2 in name_node.childNodes:
        if name_node2.nodeType == name_node2.TEXT_NODE:
            return name_node2.data
    return ""

def get_root_path(partition_name):
    dom = xml.dom.minidom.parse("../../../contents.xml")

    node = get_node(dom.documentElement.childNodes, 'builds_flat')
    for node2 in node.childNodes: 
        if node2.nodeType == node2.ELEMENT_NODE:
            if node2.tagName == "build":
                if get_nodedata(node2.childNodes, "name") == partition_name:
                    if get_nodedata(node2.childNodes, "linux_root_path") == "./":
                        return ""
                    else:
                        return get_nodedata(node2.childNodes, "linux_root_path") + "/"
    return ""

def get_org_paths():
    dom = xml.dom.minidom.parse("../../../contents.xml")
    builds_flat_node = get_node(dom.documentElement.childNodes, 'builds_flat')

    for build_node in builds_flat_node.childNodes: 
        if build_node.nodeType == build_node.ELEMENT_NODE and build_node.tagName == "build":
            root_path = get_nodedata(build_node.childNodes, "linux_root_path")
            for download_file_node in build_node.childNodes: 
                if download_file_node.nodeType == download_file_node.ELEMENT_NODE:
                    if download_file_node.tagName == "download_file" or \
                       download_file_node.tagName == "partition_file" or \
                       download_file_node.tagName == "device_programmer" or \
                       download_file_node.tagName == "partition_patch_file":
                        file_name = get_nodedata(download_file_node.childNodes, 'file_name')
                        if file_name in copy_org_file_table:
                            if root_path == "./":
                                copy_path_table.append("../../../" + get_nodedata(download_file_node.childNodes, 'file_path') + file_name)
                            else:
                                copy_path_table.append("../../../" + root_path + '/' + get_nodedata(download_file_node.childNodes, 'file_path') + file_name)

def get_sparse_file_path():
    dom = xml.dom.minidom.parse("../../../contents.xml")
    builds_flat_node = get_node(dom.documentElement.childNodes, 'builds_flat')

    for build_node in builds_flat_node.childNodes: 
        if build_node.nodeType == build_node.ELEMENT_NODE and build_node.tagName == "build":
            for download_file_node in build_node.childNodes: 
                if download_file_node.nodeType == download_file_node.ELEMENT_NODE:
                    if download_file_node.tagName == "download_file":
                        file_name = get_nodedata(download_file_node.childNodes, 'file_name')
                        if file_name == "*.img":
                            return get_nodedata(download_file_node.childNodes, 'file_path')

def get_root_paths():
    global common_root
    global linux_root
    global build_root
    global sparse_images_path
    common_root = "../../../" + get_root_path("common")
    linux_root  = "../../../" + get_root_path("apps")
    build_root  = 'common/build/'
    sparse_images_path = get_sparse_file_path()


#----------------------------------------------------------------------------
# Carry out the acquisition of the root path
#----------------------------------------------------------------------------
get_root_paths()

#----------------------------------------------------------------------------
# To set the original file name that you want to get the path.
#----------------------------------------------------------------------------
copy_org_file_table.append('NON-HLOS.bin')
copy_org_file_table.append('sbl1.mbn')
copy_org_file_table.append('sec.dat')
copy_org_file_table.append('rpm.mbn')
copy_org_file_table.append('tz.mbn')
copy_org_file_table.append('devcfg.mbn')
copy_org_file_table.append('adspso.bin')
copy_org_file_table.append('lksecapp.mbn')
copy_org_file_table.append('cmnlib_30.mbn')
copy_org_file_table.append('cmnlib64_30.mbn')
copy_org_file_table.append('keymaster64.mbn')
copy_org_file_table.append('rawprogram_unsparse.xml')
copy_org_file_table.append('prog_emmc_firehose_8953_ddr.mbn')
copy_org_file_table.append('patch0.xml')

#----------------------------------------------------------------------------
# Get the path of the original file
#----------------------------------------------------------------------------
get_org_paths()

#----------------------------------------------------------------------------
# Set the path to check the folder
#----------------------------------------------------------------------------
check_path_debug   = linux_root + '../out/debug/target/product/*'
check_path         = linux_root + '../out/target/product/*'

#----------------------------------------------------------------------------
# Set the path to fujitsu custom file
#----------------------------------------------------------------------------
# common/build
copy_path_table.append(common_root + build_root + 'gpt_main0.bin')
copy_path_table.append(common_root + build_root + 'gpt_backup0.bin')
copy_path_table.append(common_root + build_root + 'splash.img')
# sparse_images
copy_path_table.append(common_root + sparse_images_path + 'system*.img')
copy_path_table.append(common_root + sparse_images_path + 'vendor*.img')
copy_path_table.append(common_root + sparse_images_path + 'cache*.img')
copy_path_table.append(common_root + sparse_images_path + 'persist*.img')

#leifen add start - 20171024
copy_path_table.append(common_root + fsg_path + 'fsg.img')
#leifen add end - 20171024
# other
copy_path_table.append('./*.py')
copy_path_table.append('')

# userdata
copy_ud_path_table.append(common_root + sparse_images_path + 'userdata*.img')
copy_ud_path_table.append(common_root + sparse_images_path + 'userdata32G*.img')
copy_ud_path_table.append(common_root + sparse_images_path + 'userdata64G*.img')
copy_ud_path_table.append('')

# LINUX/android/out
copy_dev_path_table.append('/emmc_appsboot.mbn')
copy_dev_path_table.append('/boot.img')
copy_dev_path_table.append('/recovery.img')
copy_dev_path_table.append('/mdtp.img')
copy_dev_path_table.append('/system/build.prop')
copy_dev_path_table.append('')

