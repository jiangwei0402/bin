#!/usr/bin/python
# coding: UTF-8

import os
import fnmatch
#import get_path
import subprocess
from xml.dom.minidom import Document
import xml.etree.ElementTree as ET
import hashlib

def md5_for_file(path, block_size=256*128, hr=True):
    md5 = hashlib.md5()
    with open(path,'rb') as f:
        for chunk in iter(lambda: f.read(block_size), b''):
             md5.update(chunk)
    if hr:
        return md5.hexdigest()
    return md5.digest()
def get_partition_node(step):
	tree = ET.parse('partition.xml')
	root = tree.getroot()
	for child in root:
		for partition in root.iter('partition'):

				filename = partition.get('filename')
				if (filename == '') or (filename == ' ') or (filename == None):
					continue

				step = doc.createElement('step')
				step.setAttribute('filename',filename)
				md5num = md5_for_file(filename)

				step.setAttribute('MD5',md5num)
				step.setAttribute('filename',filename)
				step.setAttribute('operation',"flash")
				label = partition.get("label")
				step.setAttribute('partition',label)
				steps.appendChild(step)

def def_xml_tail(step):
	step1 = doc.createElement('step')
	step1.setAttribute('operation',"erase")
	step1.setAttribute('partition',"DDR")
	steps.appendChild(step1)
	step2 = doc.createElement('step')
	step2.setAttribute('operation',"oem")
	step2.setAttribute('var',"fb_mode_clear")
	steps.appendChild(step2)

def def_xml_erase(step):
	steps.removeChild(step1)
	steps.removeChild(step2)
	step = doc.createElement('step')
	step.setAttribute('operation',"erase")
	step.setAttribute('partition',"cache")
	steps.appendChild(step)
	step = doc.createElement('step')
	step.setAttribute('operation',"erase")
	step.setAttribute('partition',"userdata")
	steps.appendChild(step)
	def_xml_tail(step)


def create_servicefile_xml(step):
	def_xml_tail(step)
	doc.writexml(servicefile,indent = '\t',newl = '\n', addindent = '\t')
	servicefile.close()
	print "create servicefile.xml sucsessfully!"
	

def create_flashfile_xml(step):
	def_xml_erase(step)
	doc.writexml(flashfile,indent = '\t',newl = '\n', addindent = '\t')
	flashfile.close()
	print "create flashfile.xml sucsessfully!"

if __name__ == "__main__":
	flashfile = open('flashfile.xml','w')
	servicefile = open('servicefile.xml','w')

	doc = Document()
	flashing = doc.createElement('flashing')
	doc.appendChild(flashing)
	header = doc.createElement('header')
	flashing.appendChild(header)
	phone_model = doc.createElement('phone_model')
	phone_model.setAttribute('model',"sanders")
	header.appendChild(phone_model)
	software_version = doc.createElement('software_version')
	software_version.setAttribute('version',"sanders-user 7.1.1 NPS26.116-49 55 release-keysM8953_27.36.07.41R")
	header.appendChild(software_version)
	sparsing = doc.createElement('sparsing')
	sparsing.setAttribute('enabled',"true")
	sparsing.setAttribute('max-sparse-size',"536870912")
	header.appendChild(sparsing)
	interfaces = doc.createElement('interfaces')
	header.appendChild(interfaces)
	interface = doc.createElement('interface')
	interface.setAttribute('name',"AP")
	interfaces.appendChild(interface)
	steps = doc.createElement('steps')
	steps.setAttribute('interface',"AP")
	flashing.appendChild(steps)
	step = doc.createElement('step')
	step.setAttribute('operation',"getvar")
	step.setAttribute('var',"max-sparse-size")
	steps.appendChild(step)
	step = doc.createElement('step')
	step.setAttribute('operation',"oem")
	step.setAttribute('var',"fb_mode_set")
	steps.appendChild(step)
#set step node
	get_partition_node(step)

	step1 = doc.createElement('step')
	step1.setAttribute('operation',"erase")
	step1.setAttribute('partition',"DDR")
	steps.appendChild(step1)
	step2 = doc.createElement('step')
	step2.setAttribute('operation',"oem")
	step2.setAttribute('var',"fb_mode_clear")
	steps.appendChild(step2)
	doc.writexml(servicefile,indent = '\t',newl = '\n', addindent = '\t')
	servicefile.close()
	print "create servicefile.xml sucsessfully!"

	steps.removeChild(step1)
	steps.removeChild(step2)
	step = doc.createElement('step')
	step.setAttribute('operation',"erase")
	step.setAttribute('partition',"cache")
	steps.appendChild(step)
	step = doc.createElement('step')
	step.setAttribute('operation',"erase")
	step.setAttribute('partition',"userdata")
	steps.appendChild(step)
	def_xml_tail(step)

	doc.writexml(flashfile,indent = '\t',newl = '\n', addindent = '\t')
	flashfile.close()
	print "create flashfile.xml sucsessfully!"



