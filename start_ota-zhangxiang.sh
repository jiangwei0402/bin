#!/bin/bash

####################################################################
#
#  File Name  : start_ota.sh 
#  Description: test FOTA file on cellphone with n count
#
####################################################################


SRC_IMG_PATH=.
UPDATE_FILE=update/*.zip
DOWN_FILE=base/*.zip

PHONE_DIR=/sdcard/update.zip


#
# please change the 
# FNAME value in following line
#

count=5000; 
startid=1;
deviceID=$1
sw_version='sw_version'
action_type="upgrade"

if [ -z "$deviceID" ]; then
	deviceID=`adb devices|grep device$ | awk '{print $1}'`
fi

export ANDROID_SERIAL="$deviceID"

echo deviceId: $deviceID
echo ANDROID_SERIAL: $ANDROID_SERIAL

start_time=$(date +%Y%m%d%H%M)

if [ ! -d "${SRC_IMG_PATH}/result" ]; then
	echo "make result dir"  
	mkdir ${SRC_IMG_PATH}/result  
fi  

model=`adb shell "getprop ro.product.model"|tr -d '\r'`

if [ ! -d "${SRC_IMG_PATH}/result/${model}" ]; then
	echo "make result dir"  
	mkdir ${SRC_IMG_PATH}/result/${model}
fi  


echo "Device, Cycles, Action, Result, Power, OldVersion, NewVersion" >>${SRC_IMG_PATH}/result/${model}/${model}_${deviceID}_${start_time}.csv

for ((i=${startid}; i<=${count}; i++)) 
do 

	echo "---------------------The No $i FOTA test $(date +%Y%m%d%H%M)-------------------------"
	echo deviceID: $deviceID
	j=$((${i}%2))
	if [ $j == 1 ]; then
		FNAME=${UPDATE_FILE}
		action_type="upgrade"
	else
		FNAME=${DOWN_FILE}
		action_type="downgrade"
	fi 
	
	adb shell "echo 900000 > /sys/class/power_supply/usb/current_max"
	
	battery_level=`adb shell dumpsys batteryproperties|grep 'level'|awk -F '\\ ''{print $1}|'awk -F '\\: ' '{print $2}'|awk '{print $1}'|tr -d '\r'`
	echo "battery:" $battery_level
	if [ "$battery_level" -lt 20 ]; then
		adb shell "echo 900000 > /sys/class/power_supply/usb/current_max"
		echo "battery level is too low, charging one hours"
		sleep 3600
	fi

	echo "root phone"
	adb wait-for-device
	adb root
	sleep 5
	adb wait-for-device
	adb remount
	adb wait-for-device
	adb shell am start -n com.smartisanos.setupwizard/.SetupWizardCompleteActivity
	sleep 2
	#python usb.py
	adb wait-for-device
	
	echo "start FOTA update... by file:${FNAME}"
	sw_version=`adb shell "getprop ro.smartisan.version"|tr -d '\r'`	
	echo "step 1: push file to ${PHONE_DIR}"
	adb push ${FNAME} ${PHONE_DIR}
	adb shell "am broadcast -a android.intent.action.MEDIA_MOUNTED -d file:///sdcard"
	adb shell "echo 'boot-recovery' > /cache/recovery/command"
	adb shell "echo '--update_package=/sdcard/update.zip' >> /cache/recovery/command"
	adb shell "cat /cache/recovery/command"

	echo "step 2: enter recovery mode"
	adb reboot recovery
	sleep 30	
	echo "wait, phone is OTA progressing"
	for((z=1;z<=15;z++))
	do 
		sleep 60
		echo "waiting for device power on ..."
		checkdevice=`adb shell "dumpsys power"`
		checkPower=`echo ${checkdevice} |grep 'power'`
		if [ -n "$checkPower" ]; then
			break 1
		fi
	done
		
	adb wait-for-device
	sleep 10
	echo "if downgrade, erase userdata"
	if [ $j == 0 ];then
        	#adb wait-for-device
		#adb root
        	#adb wait-for-device
        	#adb remount
        	#adb wait-for-device
        	#adb shell "echo '--wipe_data' >> /cache/recovery/command"
		#echo "erase user data ongoing..."		
		#adb reboot recovery
		#adb reboot-bootloader
		#sleep 15
		#fastboot -w
		#sleep 5
		#fastboot reboot
		for((z=1;z<=150000;z++))
		do 
			sleep 60
			echo "waiting for device power on ..."
			checkdevice0=`adb shell "dumpsys power"`
			checkPower0=`echo ${checkdevice0} |grep 'mIsPowered=true'`
			if [ -n "$checkPower0" ]; then
				break 1
			fi
		done
	else
		for((z=1;z<=150000;z++))
		do 
			sleep 60
			echo "waiting for device power on ..."
			checkdevice1=`adb shell "dumpsys power"`
			checkPower1=`echo ${checkdevice1} |grep 'mIsPowered=true'`
			if [ -n "$checkPower1" ]; then
				break 1
			fi
		done
	fi
	adb wait-for-device
	sleep 10

	result="Power on normally"	
	screen_status=`adb shell "dumpsys power |grep 'mIsPowered=true'"`
	if [ -z "$screen_status" ]; then
		result="Power on Failed"
	fi


	echo "checked tha the device ota sucess or failed"
	new_version=`adb shell "getprop ro.smartisan.version"|tr -d '\r'`
	if [ "$sw_version" == "$new_version" ] && [ -n "$new_version" ]; then
		echo "${deviceID}, ${i}cycles, ${action_type}, Failed, ${result}, ${sw_version},${new_version}" >>${SRC_IMG_PATH}/result/${model}/${model}_${deviceID}_${start_time}.csv
	elif [ -z "$new_version" ]; then
		echo "${deviceID}, ${i}cycles, ${action_type}, Failed, ${result}, ${sw_version}" >>${SRC_IMG_PATH}/result/${model}/${model}_${deviceID}_${start_time}.csv		
	else
		echo "${deviceID}, ${i}cycles, ${action_type}, Success, ${result}, ${sw_version},${new_version}" >>${SRC_IMG_PATH}/result/${model}/${model}_${deviceID}_${start_time}.csv
	fi
	
	sleep 5
	
	echo "-----------------------------------------------------------------------------------------"
done
