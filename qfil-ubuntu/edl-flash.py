#!/usr/bin/env python

import os
import glob
import sys
import time

param_list = ['re', 'full']
fh_name = 'null'

def usage():
    print 'v1.0'
    print '[sudo] edl_flash.py re (for reflash)'
    print '[sudo] edl_flash.py full (for fullflash)'
    exit()

def check_args():
    if len(sys.argv) != 2:
        print 'need only one argv'
        usage()

    if sys.argv[1] not in param_list:
        print 'parameter only support:'
        print param_list
        usage()

def check_xml():
    xml = ['patch0.xml', 'rawprogram_unsparse_full_flash.xml', \
                        'rawprogram_unsparse.xml']
    file_list =os.listdir('./')
    if set(xml).issubset(set(file_list)):
        print 'xml exist'
    else:
        print 'no xml at all, is script in flash binary dir?'
        exit()

#prog_emmc_firehose_*
def find_fh():
    global fh_name
    for fh_name in glob.glob("prog_*_firehose_*_ddr.*"):
        print 'fh file %s' % fh_name
    if len(glob.glob("prog_*_firehose_*_ddr.*")) != 1:
        print "no fh loader or more then one, exit"
        exit()

def adb_reboot():
    if os.system("adb devices |  sed -n 2p | grep device > /dev/null") == 0:
        os.system("adb reboot edl")
        os.system("sleep 1")

def flash_img():
    if len(glob.glob('/dev/ttyUSB*')) == 0:
        print 'can not find ttySUB dev, pls adb reboot edl'
        exit()

    for ttyUSB_dev in glob.glob('/dev/ttyUSB*'):
        print 'try to flash via %s' % ttyUSB_dev
        print 'check %s access mode' % ttyUSB_dev
        if False == os.access(ttyUSB_dev, os.R_OK | os.W_OK):
            print 'Do not have access to W.R %s' % ttyUSB_dev
            print 'pls use sudo or just add $USER into \
                    dialout group: sudo adduser $USER dialout'
            usage()

        exe_args = './apps/QSaharaServer -p %s  -s 13:%s' % (ttyUSB_dev, fh_name)
        print exe_args
        if os.system(exe_args) != 0:
            exit()

    print 'sleep for a while'
    time.sleep(3)

    fullflash_exec = './apps/fh_loader --port=%s  --sendxml=patch0.xml \
            --search_path=.  --sendxml=rawprogram_unsparse_full_flash.xml \
            --search_path=. --noprompt --reset' % ttyUSB_dev

    reflash_exec = './apps/fh_loader --port=%s  --sendxml=patch0.xml \
            --search_path=.  --sendxml=rawprogram_unsparse.xml \
            --search_path=. --noprompt --reset' % ttyUSB_dev

    if sys.argv[1] == param_list[0]: #reflash
        if os.system(reflash_exec) != 0:
            exit()

    elif sys.argv[1] == param_list[1]: #full flash
        if os.system(full_exec) != 0:
            exit()

def main():
    check_args()
    check_xml()
    find_fh()
    adb_reboot()
    flash_img()

if __name__=='__main__':
    main()
