#!/usr/bin/env python
'''
@author: azhengye
'''
import difflib
import sys

_DEFAULT_CAN_REMOVE_CONFIG = [
    'CONFIG_SCHED_DEBUG\n',
    'CONFIG_DEBUG_KMEMLEAK\n',
    'CONFIG_DEBUG_KMEMLEAK_EARLY_LOG_SIZE\n',
    'CONFIG_DEBUG_KMEMLEAK_DEFAULT_OFF\n',
    'CONFIG_DEBUG_SPINLOCK\n',
    'CONFIG_DEBUG_MUTEXES\n',
    'CONFIG_DEBUG_ATOMIC_SLEEP\n',
    'CONFIG_DEBUG_STACK_USAGE\n',
    'CONFIG_DEBUG_LIST\n',
    'CONFIG_FAULT_INJECTION_DEBUG_FS\n',
    'CONFIG_LOCKUP_DETECTOR\n'
    'CONFIG_DEBUG_PAGEALLOC\n',
    'CONFIG_PAGE_POISONING\n',
    'CONFIG_RMNET_DATA_DEBUG_PKT\n',
    'CONFIG_MMC_PERF_PROFILING\n',
    'CONFIG_DEBUG_BUS_VOTER\n',
    'CONFIG_SLUB_DEBUG\n',
    'CONFIG_DEBUG_BUGVERBOSE\n',
    'CONFIG_ALLOC_BUFFERS_IN_4K_CHUNK\n',
    'CONFIG_SERIAL_CORE\n',
    'CONFIG_SERIAL_CORE_CONSOLE\n',
    'CONFIG_SERIAL_MSM_HSL\n',
    'CONFIG_SERIAL_MSM_HSL_CONSOLE\n',
    'CONFIG_MSM_TZ_LOG\n',
    'CONFIG_DYNAMIC_DEBUG\n',
    'CONFIG_ANDROID_LOGGER\n',
    'CONFIG_IMX134\n',
    'CONFIG_IMX132\n',
    'CONFIG_OV9724\n',
    'CONFIG_OV5648\n',
    'CONFIG_USB_MON\n',
    'CONFIG_USB_STORAGE_DATAFAB\n',
    'CONFIG_USB_STORAGE_FREECOM\n',
    'CONFIG_USB_STORAGE_ISD200\n',
    'CONFIG_USB_STORAGE_USBAT\n',
    'CONFIG_USB_STORAGE_SDDR09\n',
    'CONFIG_USB_STORAGE_SDDR55\n',
    'CONFIG_USB_STORAGE_JUMPSHOT\n',
    'CONFIG_USB_STORAGE_ALAUDA\n',
    'CONFIG_USB_STORAGE_KARMA\n',
    'CONFIG_USB_STORAGE_CYPRESS_ATACB\n',
    'CONFIG_SEEMP_CORE\n',
    'CONFIG_MSM_SMEM_LOGGING\n',
    'CONFIG_IOMMU_DEBUG\n',
    'CONFIG_IOMMU_DEBUG_TRACKING\n',
    'CONFIG_IOMMU_TESTS\n',
    'CONFIG_MOBICORE_DRIVER\n',
    'CONFIG_MSDOS_FS\n',
]

def main(argv):
  if len(argv) != 2:
     print '%s: invalid arguments' % argv[0]
     return 2
  filename1 = argv[1]
  try:
    with open(filename1, "r") as f1:
      str1 = f1.readlines();
      list1 =[]
      for string in str1:
          if string.startswith('#') or len(string) <= 6:
              continue
          list1.append(string.split('=')[0]+'\n')
    diffs = difflib.unified_diff(
        _DEFAULT_CAN_REMOVE_CONFIG, list1)
  except Exception as e:
    print "something wrong: %s" % e
    return 1
  status_code = 0
  for diff in diffs:
    if diff.startswith('+') or diff.startswith('-') or diff.startswith('@'):
        continue
    sys.stdout.write('follow config can be remove====>')
    sys.stdout.write(diff)
    status_code = 1
  return status_code

if __name__ == '__main__':
  sys.exit(main(sys.argv))
