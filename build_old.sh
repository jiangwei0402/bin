#!/bin/bash
#==========================================================================
# mysetenv.sh
#
# Copyright (c) 2015 by FUJITSU, Incorporated.
# All Rights Reserved.
#
#===============================================================================
# usage:  ./mysetenv.sh {modem|sbl|rpm|tz|android}
#                       modem    : modem_proc
#                       sbl      : boot_images
#                       rpm      : rpm_proc
#                       tz       : trustzone_images(tz,hyp)(Not support)
#                       android  : android

#-------------------------------------------------------------------------------
BUILD_SBL_CMD="./build.sh TARGET_FAMILY=8916 BUILD_ID=HAAAANAZ"
BUILD_RPM_CMD="./build_8916.sh"
#BUILD_TZ_CMD="./build.sh CHIPSET=msm8916 tz hyp sampleapp tzbsp_no_xpu playready widevine isdbtmm securitytest keymaster commonlib"
UPDATE_COMMON_INFO="python update_common_info.py"
BUILD_ANDROID_CMD="source build/envsetup.sh && lunch 31 && make update-api -j4 && make -j4"

##For EAAAANUZ
#BUILD_MODEM_CMD="./build.sh 8953.genns.prod -k"
#MODEM_BUILD_FLAVOR=EAAAANUZ
##For EAAAANVZ

BUILD_MODEM_CMD="./build.sh 8953.gen.prod -k BUILD_MODEL=DEBUG"
MODEM_BUILD_FLAVOR=EAAAANVZ

# arm compiler v5
arm_v5_license="8224@164.69.155.27:8224@164.69.155.28:8224@164.69.155.29"
arm_v5_root="/pkg/qct/software/arm/RVDS/5.01bld94"
armtools_v5=RVCT41
armtools_tzqsapps_v5=ARMCT5.01

# arm compiler v6
arm_v6_license="8224@164.69.155.27:8224@164.69.155.28:8224@164.69.155.29"
arm_v6_root="/pkg/qct/software/arm/RVDS/6.00bld21/sw/ARMCompiler6.00"
armtools_v6=ARMCT6

# hexagon
hexagon_root="/pkg/qct/software/hexagon/releases/tools"
hexagon_v5="5.1.05"
hexagon_v6="6.4.04"
hexagon_q6="v5"

# other
make_path="/pkg/gnu/make/3.81/bin"
sbl_root="boot_images/build/ms"
modem_root="modem_proc/build/ms"
adsp_root="adsp_proc"
sdi_root="debug_image/build/ms"
rpm_root="rpm_proc/build"
android_root="LINUX/android"
update_common_info_root="common/build"
tz_root="trustzone_images/build/ms"

#-------------------------------------------------------------------------------

#export SCONS_OVERRIDE_NUM_JOBS="1"
export ARMROOT=${arm_v5_root}
export ARMTOOLS=${armtools_v5}
export ARMLIB=$ARMROOT/lib
export ARMINCLUDE=$ARMROOT/include
export ARMINC=$ARMINCLUDE
export ARMBIN=$ARMROOT/bin64
export ARMCONF=$ARMROOT/bin
export ARMDLL=$ARMROOT/bin
export ARMBIN=$ARMROOT/bin
export ARMHOME=$ARMROOT

export HEXAGON_ROOT=${hexagon_root}
export HEXAGON_RTOS_RELEASE=${hexagon_v6}
export HEXAGON_Q6VERSION=${hexagon_q6}
export HEXAGON_IMAGE_ENTRY=0x08400000
export PATH=$MAKE_PATH:$PYTHON_PATH:$PATH:$ARMROOT:$HEXAGON_ROOT

#compile
case "$1" in 
    "modem")
        root_name=$1_root
        eval pushd \"\$$root_name\"
        if [ -e setenv.sh ]; then
            cat /dev/null > setenv.sh
        fi
        echo $(pwd)

        rm -rf bin/EAAAANVZ
        rm -rf bin/EAAAANGZ
        rm -rf bin/EAAAANYZ
        rm -rf bin/FAAAANUZ
        rm -rf bin/FAAAANWZ
        rm -rf bin/EAAAANUZ
        rm -rf bin/EAAAANWZ
        rm -rf bin/FAAAANGZ
        rm -rf bin/FAAAANVZ
        rm -rf bin/FAAAANYZ

        echo "$BUILD_MODEM_CMD"
        $BUILD_MODEM_CMD

#        if [ "$MODEM_BUILD_FLAVOR" = "EAAAANVZ" ]; then
#            cp -rf bin/$MODEM_BUILD_FLAVOR bin/EAAAANUZ
#        elif [ "$MODEM_BUILD_FLAVOR" = "EAAAANUZ" ]; then
#            cp -rf bin/$MODEM_BUILD_FLAVOR bin/EAAAANVZ
#        fi
#
#        cp -rf bin/$MODEM_BUILD_FLAVOR bin/EAAAANGZ
#        cp -rf bin/$MODEM_BUILD_FLAVOR bin/EAAAANYZ
#        cp -rf bin/$MODEM_BUILD_FLAVOR bin/FAAAANUZ
#        cp -rf bin/$MODEM_BUILD_FLAVOR bin/FAAAANWZ
#        cp -rf bin/$MODEM_BUILD_FLAVOR bin/EAAAANWZ
#        cp -rf bin/$MODEM_BUILD_FLAVOR bin/FAAAANGZ
#        cp -rf bin/$MODEM_BUILD_FLAVOR bin/FAAAANVZ
#        cp -rf bin/$MODEM_BUILD_FLAVOR bin/FAAAANYZ

        popd
        ;;

    "sbl")
        root_name=$1_root
        eval pushd \"\$$root_name\"

        if [ -e setenv.sh ]; then
            cat /dev/null > setenv.sh
        fi

        $BUILD_SBL_CMD
        popd
        ;;

    "sdi")
        echo "Not support!"
        exit
        ;;

    "tz")
        #root_name=$1_root
        #eval pushd \"\$$root_name\"
        #if [ -e setenv.sh ]; then
            #cat /dev/null > setenv.sh
        #fi
        #$BUILD_TZ_CMD
        #popd
        echo "Not support!"
        exit
        ;;

    "adsp")
        echo "Not support!"
        exit
        ;;

    "rpm")
        ##Fix the rpm build error.

        root_name=$1_root
        eval pushd \"\$$root_name\"
        if [ -e setenv.sh ]; then
            cat /dev/null > setenv.sh
        fi
        $BUILD_RPM_CMD
        popd
        ;;

    "update_common_info")
        root_name=$1_root
        eval pushd \"\$$root_name\"
        $UPDATE_COMMON_INFO
        ;;
    "android")
        root_name=$1_root
        eval pushd \"\$$root_name\"
        eval $BUILD_ANDROID_CMD
        popd
        ;;

    "cnss"|"tzqsapps")
        echo "Not support!"
        exit
        ;;

    "NON-HLOS")
        ${0} modem
        ${0} update_common_info
        exit
        ;;

    "all")
        ${0} sbl && \
        ${0} rpm && \
        ${0} modem && \
        ${0} android && \
        ${0} update_common_info
        exit
        ;;

    *)
        echo "./mysetenv.sh {all|modem|sbl|rpm|tz|android|update_common_info|NON-HLOS}"
        exit
        ;;
esac
