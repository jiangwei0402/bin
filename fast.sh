#!/bin/bash - 
#===============================================================================
#
#          FILE: fast.sh
# 
#         USAGE: ./fast.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 2015年09月18日 10:18
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
sudo adb reboot bootloader
sudo fastboot flash boot ~/dir/squash/LINUX/android/out/debug/target/product/F05F/boot.img 
sudo fastboot flash recovery ~/dir/squash/LINUX/android/out/debug/target/product/F05F/recovery.img 
sudo fastboot reboot

