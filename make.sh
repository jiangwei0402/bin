#!/bin/bash -x


MAKEFLGJ=-j8
export CCACHE_DIR=/home/zx/dir/.ccache
export USE_CCACHE=1

TIME=`date +%y%m%d-%H%M`

#watch -n1 -d prebuilts/misc/linux-x86/ccache/ccache -s 

#cp ~/bin/ccache prebuilts/misc/linux-x86/ccache
./prebuilts/misc/linux-x86/ccache/ccache -M 50G

if [ ! -d log ];
then
	mkdir log 
fi

if [[ -z $1 ]];
then
#	make update-api ${MAKEFLGJ} 2>&1 |tee "log/update-api-${TIME}.log"
	make ${MAKEFLGJ} 2>&1 |tee "log/make-full-${TIME}.txt"
else
	make $1 ${MAKEFLGJ} 2>&1 |tee "log/$1-${TIME}.txt"
fi

#eject
