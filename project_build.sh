#!/bin/bash
#===============================================================================
#
# Copyright © 2008-2015 Thundersoft
#
# build shell script file.
#
#     Author: lu tianxiang (scm)
#     E-mail: lutx0528@thundersoft.com
#     Date  : 04/05/2016
#
#-------------------------------------------------------------------------------
#
#                      EDIT HISTORY FOR FILE
#
# This section contains comments describing changes made to the module.
# Notice that changes are listed in reverse chronological order.
#
# when       who           what, where, why
# --------   ---           -----------------------------------------------------
# 04/05/2016 lu tianxiang  Create modem build script
# 02/20/2017 lu tianxiang  update for script version to 1.0
#===============================================================================

version(){
  echo "1.5"
}

usage(){
echo "
usage : project_build [<option> <option> ...] <parameter>

example :
  1.build android and modem and create 1ppon, like this.
      ./project_build.sh --all --common-info --1ppon user ${build_product}

  2.remove out-dir befor build android, like this.
      ./project_build.sh --rm-out --all user ${build_product}

  3.update-api befor android build, like this.
      ./project_build.sh --api --all user ${build_product}

  4.change android choosecombo option, like this.
      ./project_build.sh --api --all eng ${build_product}

  5.build android only , like this.
      ./project_build.sh -d eng ${build_product}

parameters :
 <package>  : 作成するパッケージのタイプを指定　[default:eng]
 <product>  : name of the android build product.[default:${build_product}]

 optional arguments:
  -h, --help        print this help and exits
  -v, --version     print version and exits
  -a, --adsp        build adsp only
  -b, --boot        build boot only
  -m, --modem       build modem only
  -r, --rpm         build rpm only
  -t, --tz          build trustzone only
  -d, --android     build droid only
  --amss            build all amss module
  --nv              build nv_bin
  --rm-out          remove out dir befor build android
  --api             build droid with update-api
  --all             build all amss and droid
  --roms            make romimages
  --common-info     execute update_common_info only
  --1ppon           package 1ppon only
"
}

getargs(){
  index=0
  boot=
  modem=
  rpm=
  tz=
  adsp=
  sdi=
  nvbin=
  droid=
  build_all=
  api=
  ppon=
  roms=
  ucinfo=
  rmout=
  package_type=userdebug
  build_product=msm9853_64
  droid_module=all
  BIN_USER=
  BIN_NOUSER=
  ROM_FILE=
  for parameter in $* ;do
    start=$(expr match "${parameter}" '-\|--')
    option=${parameter:$start}
    if [[ $start -gt 0 ]];then
      if [[ "$option" == "h" || "$option" == "help" ]];then
        usage && exit 0
      elif [[ "$option" == "v" || "$option" == "version" ]];then
        version && exit 0
      elif [[ "$option" == "b" || "$option" == "boot" ]];then
        boot=1
      elif [[ "$option" == "m" || "$option" == "modem" ]];then
        modem=1
      elif [[ "$option" == "r" || "$option" == "rpm" ]];then
        rpm=1
      elif [[ "$option" == "t" || "$option" == "tz" ]];then
        tz=1
      elif [[ "$option" == "a" || "$option" == "adsp" ]];then
        adsp=1
      elif [[ "$option" == "s" || "$option" == "sdi" ]];then
        sdi=1
      elif [[ "$option" == "d" || "$option" == "android" ]];then
        droid=1
      elif [[ "$option" == "amss" ]];then
        boot=1
        modem=1
        rpm=1
        tz=1
        adsp=1
        sdi=1
      elif [[ "$option" == "nv" ]];then
        nvbin=1
      elif [[ "$option" == "all" ]];then
        build_all=1
        boot=1
        modem=1
        rpm=1
        tz=1
        adsp=1
        sdi=1
        nvbin=1
        droid=1
      elif [[ "$option" == "rm-out" ]];then
        rmout=1
      elif [[ "$option" == "api" ]];then
        api=1
      elif [[ "$option" == "roms" ]];then
        roms=1
      elif [[ "$option" == "common-info" ]];then
        roms=1
        ucinfo=1
      elif [[ "$option" == "1ppon" ]];then
        ppon=1
      else
        echo -e "\033[31munvalid option $parameter.\033[0m"
        usage && exit 0
      fi
    elif [[ ${parameter:0:1} != '-' ]];then
      if [[ $index -eq 0 ]];then package_type=$parameter;fi
      if [[ $index -eq 1 ]];then build_product=$parameter;fi
      if [[ $index -eq 2 ]];then droid_module=$parameter;fi
      ((index++))
    else
      echo "!!unvalid parameter '$parameter' !!\n"
    fi
  done
  if [[ -z $boot && -z $modem && -z $rpm && -z $tz && -z $adsp &&
        -z $rmout && -z $droid && -z $api && -z $ppon && -z $ucinfo &&
        -z $nvbin && -z $roms ]]
  then
    echo -e "\033[31moptions must choose.\033[0m"
    usage && exit 0
  fi
  #if [[ $index == 0 ]];then
  #  echo -e "\033[31mproduct must entry.\033[0m"
  #  usage && exit 0
  #fi
}

#===============================================================================
# fail msg
#===============================================================================
fail () {
  if [ ! -z "$@" ]
  then
    echo -e "\033[31mERROR: $@\033[0m" >&2
  fi
  echo -e "\033[31mERROR: failed.\033[0m" >&2
  usage
  exit 1
}

#===============================================================================
# set up common environment
#===============================================================================
export MAKE_PATH=/usr/bin
#MODEM_BUILD_FLAVOR=EAAAANVZ

#===============================================================================
# set up Hexagon environment
#===============================================================================
export_hexagon3(){
  export HEXAGON_ROOT=/pkg/qct/software/hexagon/releases/tools
  export HEXAGON_RTOS_RELEASE=1.1
  export HEXAGON_Q6VERSION=v55
}
export_hexagon5(){
  export HEXAGON_ROOT=/pkg/qct/software/hexagon/releases/tools
  export HEXAGON_RTOS_RELEASE=5.1.05
  export HEXAGON_Q6VERSION=v5
}
export_hexagon6(){
  export HEXAGON_ROOT=/pkg/qct/software/hexagon/releases/tools
  export HEXAGON_RTOS_RELEASE=6.4.04
  export HEXAGON_Q6VERSION=v5
  export HEXAGON_IMAGE_ENTRY=0x08400000
}

#===============================================================================
# set up ARM environment
#===============================================================================
export_ARM6(){
  export SCONS_OVERRIDE_NUM_JOBS="1"
  export ARMROOT=/pkg/qct/software/arm/RVDS/6.00bld21/sw/ARMCompiler6.00
  if [ "${ARM6_LICENSE_FILE}" != "" ] ; then
    export ARMLMD_LICENSE_FILE=${ARM6_LICENSE_FILE}
  else
    export ARMLMD_LICENSE_FILE=${ARM6_LICENSE_FILE}
#    export ARMLMD_LICENSE_FILE=8224@164.69.140.27:8224@164.69.140.28:8224@164.69.140.29
  fi
  export ARMTOOLS="ARMCT6"
  export ARMHOME=$ARMROOT
  export ARMCOMPILER6_ASMOPT=--tool_variant=ult
  export ARMCOMPILER6_CLANGOPT=--tool_variant=ult
  export ARMCOMPILER6_FROMELFOPT=--tool_variant=ult
  export ARMCOMPILER6_LINKOPT=--tool_variant=ult
  #export_hexagon6
  export PATH=/pkg/qct/software/python/2.7.6/bin:$PATH
  export PATH=$ARM_COMPILER_PATH:$ARMROOT:$HEXAGON_ROOT:$PATH
}

export_ARM5(){
  export SCONS_OVERRIDE_NUM_JOBS="24"
  if [ "${ARM5_LICENSE_FILE}" != "" ] ; then
    export ARMLMD_LICENSE_FILE=${ARM5_LICENSE_FILE}
  else
    export ARMLMD_LICENSE_FILE=8224@164.69.140.27:8224@164.69.140.28:8224@164.69.140.29
  fi
  export ARMTOOLS="RVCT41"
  export ARMROOT="/pkg/qct/software/arm/RVDS/ARM_Compiler_5"
  export ARM_COMPILER_PATH=/usr/local/ARM_Compiler_5/bin64
  export ARMBIN=$ARMROOT/bin64
  export ARMLIB=$ARMROOT/lib
  export ARMINCLUDE=$ARMROOT/include
  export ARMINC=$ARMINCLUDE
  export ARMHOME=$ARMROOT
  export ARMPATH=$ARMBIN
  #export_hexagon5
  export PATH=$ARM_COMPILER_PATH:$ARMROOT:$HEXAGON_ROOT:$PATH
}

#===============================================================================
# set up ARM environment
#===============================================================================
export_LLVM(){
  export LLVMTOOLS=LLVM
  export LLVMROOT=/pkg/qct/software/llvm/release/arm/3.5.2.4
  export LLVMBIN=$LLVMROOT/bin
  export ARM_COMPILER_PATH=$LLVMBIN
  export LLVMLIB=$LLVMROOT/lib/clang/3.5.2/lib/linux
  export MUSLPATH=$LLVMROOT/tools/lib64
  export MUSL32PATH=$LLVMROOT/tools/lib32
  export LLVMINC=$MUSLPATH/include
  export LLVM32INC=$MUSL32PATH/include
  export LLVMTOOLPATH=$LLVMROOT/tools/bin
  export GNUROOT=/pkg/qct/software/arm/linaro-toolchain/aarch64-none-elf
  export GNUARM7=/pkg/qct/software/arm/linaro-toolchain/gcc-linaro-arm-linux-gnueabihf-4.8-2014.04_linux
}

#===============================================================================
# set up java environment
#===============================================================================
export_JDK7(){
  if [ "${JAVA_HOME}" == "" ] ; then
    if [ -d /opt/tsenv/java-7-openjdk-amd64/ ] ; then
      export JAVA_HOME=/opt/tsenv/java-7-openjdk-amd64/
    elif [ -d /usr/lib/jvm/java-7-openjdk-amd64/ ] ; then
      export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64/
    else
      export JAVA_HOME=/opt/tsenv/java-7-openjdk-amd64/
    fi
  fi
  export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
  export PATH=$JAVA_HOME/bin:~/bin:~/bin/tool:$PATH
}

export_JDK8(){
  export JAVA_HOME=/opt/tsenv/java-8-openjdk-amd64/
  if [ "${JAVA_HOME}" == "" ] ; then
    if [ -d /opt/tsenv/java-8-openjdk-amd64/ ] ; then
      export JAVA_HOME=/opt/tsenv/java-8-openjdk-amd64/
    elif [ -d /usr/lib/jvm/java-8-openjdk-amd64/ ] ; then
      export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
    else
      export JAVA_HOME=/opt/tsenv/java-8-openjdk-amd64/
    fi
  fi
  export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
  export PATH=$JAVA_HOME/bin:~/bin:~/bin/tool:$PATH
}

#===============================================================================
# build boot image
#===============================================================================
build_boot(){
  cd $1/BOOT.BF.3.3/boot_images/build/ms
  chmod +x build.sh
  export_ARM5
  echo "./build.sh TARGET_FAMILY=8953 FJDEV_ID=FJDEV048 --prod"
  ./build.sh TARGET_FAMILY=8953 FJDEV_ID=FJDEV048 --prod
  local result=$?
  if [ $result -ne 0 ]; then
    fail "build boot_image fail"
  fi
  echo "" >> $1/build-log.txt
  cd $1
}

#===============================================================================
# build modem image
#===============================================================================
build_modem(){
  cd $1/MPSS.TA.2.3/modem_proc/build/ms
  if [ -e setenv.sh ]; then
    cat /dev/null > setenv.sh
  fi
  #export PYTHON_PATH=/pkg/qct/software/python/2.7.6/bin
  export PATH=/pkg/qct/software/python/2.7.6/bin:$PATH
  export_ARM5
  export_hexagon3
  ./build.sh 8953.gen.prod BUILD_MODEL=DEBUG BUILD_CODE=7089 -k
  echo "./build.sh 8953.gen.prod BUILD_MODEL=DEBUG BUILD_CODE=7089 -k"
  local result=$?
  if [ $result -ne 0 ]; then
    fail "build modem_proc fail"
  fi
  cd $1
}

#===============================================================================
# build rpm image
#===============================================================================
build_rpm(){
  cd $1/RPM.BF.2.4/rpm_proc/build
  export_ARM5
  ./build_8953.sh
  local result=$?
  if [ $result -ne 0 ]; then
    fail "build rpm fail"
  fi
  echo "./build_8x10.sh"
  cd $1
}

#===============================================================================
# build trustzone image
#===============================================================================
build_tz(){
  cd $1/TZ.BF.4.0.5/trustzone_images/build/ms
  export_ARM6
  export_hexagon6
  export_LLVM
  unset PYTHONPATH
  export PATH=$ARM_COMPILER_PATH:$ARMROOT:$HEXAGON_ROOT:$PATH
  ./build.sh CHIPSET=msm8953 devcfg -c
  local result=$?
  if [ $result -ne 0 ]; then
    fail "build tz fail"
  fi
  echo "./build.sh CHIPSET=msm8953 devcfg sampleapp"
  ./build.sh CHIPSET=msm8953 devcfg sampleapp
  local result=$?
  if [ $result -ne 0 ]; then
    fail "build tz fail"
  fi
  echo "./build.sh CHIPSET=msm8953 devcfg"
  cd $1
}

#===============================================================================
# build adsp image
#===============================================================================
build_adsp(){
  cd $1/ADSP.8953.2.8.2/adsp_proc/build
  python ./build.py -c msm8953 -o all
  local result=$?
  if [ $result -ne 0 ]; then
    fail "build adsp fail"
  fi
  echo "python ./build.py -c msm8953 -o all"
  cd $1
}

#===============================================================================
# build android
#===============================================================================
clean_kernel(){
  cd $1/LA.UM.5.6/LINUX/android
  _kernel_sha1="$(git log -1 --pretty=%H)"
  cd $1
}
reset_kernel(){
  cd $1/LA.UM.5.6/LINUX/android
  git reset --hard $2
  cd $1
}
build_droid(){
  cd $1/LA.UM.5.6/LINUX/android
  export_JDK8
  export JAVA_OPTS='-Xmx4096M'
  source build/envsetup.sh
  echo "source build/envsetup.sh"
  lunch ${build_product}-${build_type}

  ret=0
  cpu=$(grep -c 'processor' /proc/cpuinfo)
  if [[ $api -eq 1 ]];then
    make update-api -j${cpu} -k -i
    ret=$?
    echo "make update-api -j${cpu}"
  fi
  if [ -z $droid_module ]
  then
    make -j${cpu}
    ret=$?
    echo "make -j${cpu}"
    if [ $ret -ne 0 ]; then
      fail "build android fail"
    fi
  elif [ $droid_module == "all" ]
  then
    make -j${cpu}
    ret=$?
    echo "make -j${cpu}"
    if [ $ret -ne 0 ]; then
      fail "build android fail"
    fi
  else
    make -j${cpu} $droid_module
    ret=$?
    echo "make -j${cpu} ${droid_module}"
    if [ $ret -ne 0 ]; then
      fail "build ${droid_module} fail"
    fi
  fi
  if [ -z $droid_module ] || [ $droid_module == "all" ];then
    echo "make -j${cpu} oem_image"
    make -j${cpu} oem_image
    ret=$?
  fi
  cd $1
  if [ $ret -ne 0 ]; then
    fail "build android fail"
  fi
}

#===============================================================================
# update_common_info
#===============================================================================
update_common_info(){
  OUTDIR=LA.UM.5.6/LINUX/android/out
  cd $1/MSM8953.LA.2.0/common/build
  python build.py
  local result=$?
  if [ $result -ne 0 ]; then
    fail "update_common_info fail"
  fi
  echo "python build.py"
  cd $1
}

#===============================================================================
# build 1ppon
#===============================================================================
build_1ppon(){
  cd $1/MSM8953.LA.2.0/common/tools/1ppon
  ret=0
  if [[ $build_type == "debug" ]]; then
    echo "BinFilesCollect_debug.sh"
    ./BinFilesCollect_debug.sh
    ret=$?
  else
    echo "BinFilesCollect.sh"
    ./BinFilesCollect.sh
    ret=$?
  fi
  if [ $ret -ne 0 ]; then
    fail "BinFilesCollect fail"
  fi
  cd $1/BOOT.BF.3.3/boot_images/build/ms/bin/JAADANAZ/
  tar -c prog_emmc_firehose_8953_ddr.mbn \
    | tar -x -C $1/MSM8953.LA.2.0/common/tools/1ppon/32G/
  cd $1/MSM8953.LA.2.0/common/tools/1ppon/32G
  ls > ../list1
  wine ../BinaryJointTool.exe
  local result=$?
  if [ $result -ne 0 ]; then
    fail "BinaryJointTool.exe fail"
  fi
  echo "wine BinaryJointTool.exe"
  ls > ../list2
  pponfile=`diff ../list1 ../list2 | grep "^>" | awk '{print $2}'`
  if [ -z ${pponfile} ]
  then
    cd $1
    return
  fi
  mv ${pponfile} ${BIN_USER}.bin
  md5sum ${BIN_USER}.bin > $1/bin.md5sum
  zip $1/${BIN_USER}.zip ${BIN_USER}.bin || fail "archive ppon error"
  rm -rf ${BIN_USER}.bin
  echo "${BIN_USER}.zip" | tee -a $1/archive.list
  cd $1/MSM8953.LA.2.0/common/build/
  tar -c *.sh *.bat *.py | tar -x -C $1/MSM8953.LA.2.0/common/tools/1ppon/32G
  cd $1/MSM8953.LA.2.0/common/tools/1ppon/32G
  mkdir -p $1/${QFIL_FILES}
  tar -c * | tar -x -C $1/${QFIL_FILES}
  cd $1/${QFIL_FILES}
  rm -rf f16golfz32*.bin
  cd $1
  zip -r $1/${QFIL_FILES}.zip ${QFIL_FILES} || fail "archive qfil error"
  rm -rf $1/${QFIL_FILES}
  echo "${QFILE_FILES}.zip" | tee -a $1/archive.list
  ### create nouser bin
  #sed -i 's/^userdata/\#userdata/g' select.txt
  #python select.py
  #ls > ../list1a
  #wine ../BinaryJointTool.exe
  #local result=$?
  #if [ $result -ne 0 ]; then
  #  fail "BinaryJointTool.exe fail"
  #fi
  #echo "wine BinaryJointTool.exe"
  #ls > ../list2a
  #ponfile=`diff ../list1a ../list2a | grep "^>" | awk '{print $2}'`
  #if [ -z ${pponfile} ]; then
  #  cd $1
  #  return
  #fi
  #cd $1
  #mv ${pponfile} ${BIN_NOUSER}.bin
  #zip $1/${BIN_NOUSER}.zip ${BIN_NOUSER}.bin || fail "archive ppon error"
  #rm -rf ${BIN_NOUSER}.bin
  #echo "${BIN_NOUSER}.zip" | tee -a $1/archive.list
  cd $1
}

#===============================================================================
# build roms 
#===============================================================================
build_roms(){
  cd $1
  OUTDIR=LA.UM.5.6/LINUX/android/out

  ./make_rom.sh ${build_type} ${build_product} ${ROM_FILE}
  echo "${ROM_FILE}.zip" | tee -a $1/archive.list
  cd $1
}

#===============================================================================
# build lsm off
#===============================================================================
build_nvbin(){
  cd $1
  cd $1
}

#===============================================================================
# reset version
#===============================================================================
build_version(){
  cd $1
  cd $1/LA.UM.5.6/LINUX/android/device/fujitsu/${build_product}
  _vresion_sha1="$(git log -1 --pretty=%H)"
  version_now=$(grep BUILD_DISPID version.mk | head -1 | awk '{print $3}')
  version_new=
  case "${package_type}" in
    "AMTS")
      version_new=${version_now:3:5}-AMTS
      ;;
    "userdebug")
      version_new=${version_now:3:5}-UD
      ;;
    "USER-ST")
      version_new=${version_now:3:5}-ST
      ;;
    "eng")
      version_new=${version_now:3:5}e
      ;;
    *)
      version_new=${version_now:3:5}
      ;;
  esac
  cmd_str=$(printf "sed -i 's/${version_now}/${version_new}/g' version.mk\n")
  eval ${cmd_str}
  need_commit=$(git diff --name-status | wc -l)
  if [ ${need_commit} -ne 0 ]; then
    git add version.mk
    git commit -m 'set version for build'
  fi
  cd $1
}
unreset_version(){
  cd $1/LA.UM.5.6/LINUX/android/device/fujitsu/${build_product}
  git reset --hard $2
  cd $1
}


#===============================================================================
# main
#===============================================================================
getargs $*

build_dir=$(cd $(dirname $0); echo $(pwd))

> ${build_dir}/env.list
source environment.sh

setenv=`export`
starttime="$(date +%s)"
starttimefmt=`date --date='@'$starttime`

#=============================================================================
# Setup Paths
#===============================================================================
#build_dir=`dirname $0`
touch ${build_dir}/archive.list
project_dir=${build_dir%/amss*}

#export BOOT_ROOT=$build_dir/modem_proc/build/ms
#VENDOR_ROOT=$build_dir/vendor

#===============================================================================
# Set target enviroment
#===============================================================================
echo "Start Time = $starttimefmt" > build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# ENVIRONMENT BEGIN" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt

#export ARMLMD_LICENSE_FILE="8224@164.69.140.27:8224@164.69.140.28:8224@164.69.140.29"
export MAKE_PATH="/pkg/gnu/make/3.81/bin"
export PATH=$MAKE_PATH:$PATH

if [[ $rmout -eq 1 ]];then
  echo "# remove out dir" >> build-log.txt
  echo "rm -rf LA.UM.5.6/LINUX/android/out"
  rm -rf ${build_dir}/LA.UM.5.6/LINUX/android/out
fi

if [ "${build_type}" = "release" ] ;
then
  cp ${build_dir}/MSM8953.LA.2.0/contents_rel.xml ${build_dir}/MSM8953.LA.2.0/contents.xml
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# ENVIRONMENT END" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt

echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# GLOBAL ENV BEGIN" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
VERSION_ANDROID="#${PROJ}_${build_product}_$(date +%Y%m%d_%H%M%S)_#${VERSION_ID}"
VERSION_AMSS=""
VFILE_ANDROID="${build_dir}/LA.UM.5.6/LINUX/android/device/qcom/${build_product}/version.mk"
VFILE_AMSS="${build_dir}/MPSS.TA.2.3/modem_proc/core/api/oem_services/rom_vers.h"
if [ -f ${VFILE_ANDROID} ]
then
  VERSION_ANDROID=$(grep BUILD_DISPID ${VFILE_ANDROID} | head -n 1 | awk '{print $3}')
fi
if [ -f ${VFILE_AMSS} ]
then
  VERSION_AMSS=$(grep ROM_VERSION ${VFILE_AMSS} | head -n 1 | awk '{print $3}')
fi
case "${package_type}" in
  "eng")
    BIN_USER="${build_product}_${VERSION_ANDROID}e"
    BIN_NOUSER="${build_product}_nouser_${VERSION_ANDROID}e"
    ROM_FILE="romimage_golf_n_${VERSION_ANDROID}e"
    QFIL_FILES="qfil_golf_n_${VERSION_ANDROID}e"
    ;;
  "user")
    BIN_USER="${build_product}_${VERSION_ANDROID}"
    BIN_NOUSER="${build_product}_nouser_${VERSION_ANDROID}"
    ROM_FILE="romimage_golf_n_${VERSION_ANDROID}"
    QFIL_FILES="qfil_golf_n_${VERSION_ANDROID}"
    ;;
  "userdebug")
    BIN_USER="${build_product}_${VERSION_ANDROID}-UD"
    BIN_NOUSER="${build_product}_nouser_${VERSION_ANDROID}-UD"
    ROM_FILE="romimage_golf_n_${VERSION_ANDROID}-UD"
    QFIL_FILES="qfil_golf_n_${VERSION_ANDROID}-UD"
    ;;
  "USER")
    BIN_USER="${build_product}_${VERSION_ANDROID}"
    BIN_NOUSER="${build_product}_nouser_${VERSION_ANDROID}"
    ROM_FILE="romimage_golf_n_${VERSION_ANDROID}"
    QFIL_FILES="qfil_golf_n_${VERSION_ANDROID}"
    ;;
  "USER-ST")
    BIN_USER="${build_product}_${VERSION_ANDROID}-ST"
    BIN_NOUSER="${build_product}_nouser_${VERSION_ANDROID}-ST"
    ROM_FILE="romimage_golf_n_${VERSION_ANDROID}-ST"
    QFIL_FILES="qfil_golf_n_${VERSION_ANDROID}-ST"
    ;;
  "AMTS")
    BIN_USER="${build_product}_${VERSION_ANDROID}-AMTS"
    BIN_NOUSER="${build_product}_nouser_${VERSION_ANDROID}-AMTS"
    ROM_FILE="romimage_golf_n_${VERSION_ANDROID}-AMTS"
    QFIL_FILES="qfil_golf_n_${VERSION_ANDROID}-AMTS"
    ;;
  "DOU")
    BIN_USER="${build_product}_${VERSION_ANDROID}-DOU"
    BIN_NOUSER="${build_product}_nouser_${VERSION_ANDROID}-DOU"
    ROM_FILE="romimage_golf_n_${VERSION_ANDROID}-DOU"
    QFIL_FILES="qfil_golf_n_${VERSION_ANDROID}-DOU"
    ;;
  *)
    BIN_USER="${build_product}_${VERSION_ANDROID}"
    BIN_NOUSER="${build_product}_nouser_${VERSION_ANDROID}"
    ROM_FILE="romimage_golf_n_${VERSION_ANDROID}"
    QFIL_FILES="qfil_golf_n_${VERSION_ANDROID}"
    ;;
esac
echo "#-------------------------------------------------------------------------------" >> build-log.txt

echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# BUILD BEGIN" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt

echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $boot -eq 1 ]];then
  echo "# Build boot image" >> build-log.txt
  build_boot $build_dir 2>&1 | tee -a build-log.txt
  result=${PIPESTATUS[0]}
  if [ "$result" != "0" ]; then exit $result ;fi
else
  echo "###IGNORE build boot image" >> build-log.txt
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $rpm -eq 1 ]];then
  echo "# Build rpm image" >> build-log.txt
  build_rpm $build_dir 2>&1 | tee -a build-log.txt
  result=${PIPESTATUS[0]}
  if [ "$result" != "0" ]; then exit $result ;fi
else
  echo "###IGNORE build rpm image" >> build-log.txt
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $modem -eq 1 ]];then
  echo "# Build modem image" >> build-log.txt
  build_modem $build_dir 2>&1 | tee -a build-log.txt
  result=${PIPESTATUS[0]}
  if [ "$result" != "0" ]; then exit $result ;fi
else
  echo "###IGNORE build modem image" >> build-log.txt
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $tz -eq 1 ]];then
  echo "# Build tz image" >> build-log.txt
  build_tz $build_dir 2>&1 | tee -a build-log.txt
  result=${PIPESTATUS[0]}
  if [ "$result" != "0" ]; then exit $result ;fi
else
  echo "###IGNORE build trustzone image" >> build-log.txt
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $adsp -eq 1 ]];then
  echo "# Build adsp image" >> build-log.txt
  build_adsp $build_dir 2>&1 | tee -a build-log.txt
  result=${PIPESTATUS[0]}
  if [ "$result" != "0" ]; then exit $result ;fi
else
  echo "###IGNORE build adsp image" >> build-log.txt
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $droid -eq 1 ]];then
  echo "# Build droid image" >> build-log.txt
  _kernel_sha1=
  _version_sha1=
  clean_kernel $build_dir
  reset_version $build_dir
  build_droid $build_dir 2>&1 | tee -a build-log.txt
  result=${PIPESTATUS[0]}
  reset_kernel $build_dir "${_kernel_sha1}"
  unreset_version $build_dir "${_version_sha1}"
  if [ "$result" != "0" ]; then exit $result ;fi
else
  echo "###IGNORE build droid image" >> build-log.txt
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $nvbin -eq 1 ]];then
  ## interface for build_nvbin
  echo ""
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $roms -eq 1 ]];then
  echo "# build_roms" >> build-log.txt
  build_roms $build_dir 2>&1 | tee -a build-log.txt
  result=${PIPESTATUS[0]}
  if [ "$result" != "0" ]; then exit $result ; fi
else
  echo "###IGNORE make roms" >> build-log.txt
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $ucinfo -eq 1 ]];then
  echo "# update common info" >> build-log.txt
  update_common_info $build_dir 2>&1 | tee -a build-log.txt
  result=${PIPESTATUS[0]}
  if [ "$result" != "0" ]; then exit $result ;fi
else
  echo "###IGNORE update common info" >> build-log.txt
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $ppon -eq 1 ]];then
  echo "# create 1ppon" >> build-log.txt
  build_1ppon $build_dir 2>&1 | tee -a build-log.txt
  result=${PIPESTATUS[0]}
  if [ "$result" != "0" ]; then exit $result ;fi
else
  echo "###IGNORE create 1ppon" >> build-log.txt
fi

echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# BUILD END" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt

endtime="$(date +%s)"
endtimefmt=`date --date='@'$endtime`
elapsedtime=$(expr $endtime - $starttime)
echo
echo "Start Time = $starttimefmt - End Time = $endtimefmt" >> build-log.txt
echo "Elapsed Time = $elapsedtime seconds" >> build-log.txt

echo "Start Time = $starttimefmt - End Time = $endtimefmt"
echo "Elapsed Time = $elapsedtime seconds"
