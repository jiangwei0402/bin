#!/bin/bash

REPO=/usr/bin/repo

#TIME=`date +%y%m%d-%H%M`
TIME=`date '+%Y-%m-%d %H:%M:%S %A'`
#SOURCE_PATH=/mnt/M-OSV/Darts-M/

#cd $SOURCE_PATH
#$REPO forall -c 'git clean -fd;git checkout -f' 2>&1| tee -a x
#$REPO sync -j4 2>&1|tee -a x

#SOURCE_PATH=/home/guest/ligf/source/ncpu/mango
#REPO=/home/guest/ligf/bin/repo

#cd $SOURCE_PATH
#$REPO forall -c 'git clean -fd;git checkout -f' 2>&1| tee -a x
#$REPO sync -j4 2>&1|tee -a x


clean-project()
{
	if [ -d .repo ]; then
		${REPO} start master --all
		${REPO} sync -j4 |tee repo.log \
			&& echo $TIME >> repo.log
	else
		echo "repository error !!!!!!!"
		echo "Error $(pwd) \nRepository error: can't find .repo at $(pwd)" | mail -S 616618Lan-xiang.zhang@ontim.cn -s "[REPO][ERROR]$(pwd)" xiang.zhang@ontim.cn 
	fi
}

repo-sync()
{
	pushd $1
	clean-project
	popd $1
}

#repo forall -c 'commitID=`git log --before "2018-04-04 12:00" -1 --pretty=format:"%H"`; git reset --hard $commitID'

