#!/bin/bash
echo "复制$1列表中的文件树"

FILE_LIST="$1"
TARG_DIR="./cp-target-folder-`date '+%m-%d_%H:%M'`"

if [ -f $FILE_LIST ] ; then
	mkdir -p $TARG_DIR
	echo "复制列表中的文件树到指定目录：$TARG_DIR"

	count=0
	while read -r FILE_NAME
	do
		if [ -f $FILE_NAME ] ; then
			
			cp --parents $FILE_NAME $TARG_DIR
			if [ $? -ne 0 ]; then
				echo "ERROR：复制文件[$FILE_NAME]错误。"
				continue
			fi
			#count=`expr $count + 1`
			count=$(($count+1))
		else
			echo "$FILE_NAME文件不存在！"
		fi
	done < $FILE_LIST
else
	echo "Error:文件列表$FILE_LIST不存在!"
	exit 1;
fi
echo "复制完成，共复制$count个文件到目标目录。"


