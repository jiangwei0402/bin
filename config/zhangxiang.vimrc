" .vimrc - Vim configuration file.
"
" Copyright (c) 2010 Jeffy Du. All Rights Reserved.
"
" Maintainer: Jeffy Du <jeffy.du@gmail.com>
"    Created: 2010-01-01
" LastChange: 2011-01-09

" GENERAL SETTINGS:
" To use VIM settings, out of VI compatible mode.
set nocompatible
" Enable file type detection.
filetype plugin indent on
" Syntax highlighting.
syntax enable
syntax on
set number

" Setting colorscheme
color evening
"If the evening color is error. Pls change the background=light
"set t_Co=17
" Config for vim72
if v:version >= 702
set updatetime=1000
set   autoindent
set   autoread
set   autowrite
set   background=dark
set   backspace=indent,eol,start
set nobackup
set   cindent
set   cinoptions=:0
set   completeopt=longest,menuone
set   cursorline
set   encoding=utf-8
"set noexpandtab
set   fileformat=unix
set   fileencodings=utf-8,shift-jis,euc-jp,utf-8,gb2312,gbk,gb18030,chinese

set   foldenable
set   foldmethod=marker
set   helpheight=10
set   helplang=cn
set   hidden
set   history=100
set   hlsearch
"set   ignorecase
set   incsearch
set   laststatus=2
set   mouse=a
set   number
set   pumheight=10
set   ruler
set   scrolloff=5
set   shiftwidth=4
set   showcmd
set   smartindent
set   smartcase
set   tabstop=4
"set   termencoding=utf-8
set   textwidth=9999
set   whichwrap=h,l
set   wildignore=*.bak,*.o,*.e,*~
set   wildmenu
set   wildmode=list:longest,full
set nowrap
set autochdir
endif
" Config for vim73
if v:version >= 703
set   colorcolumn=+1
endif
" Config for win32 gvim.
if has("win32")
set   guioptions-=T
set   guioptions-=m
set   guioptions-=r
set   guioptions-=l
set   lines=26
set   columns=90
source $VIMRUNTIME/delmenu.vim
source $VIMRUNTIME/menu.vim
language messages zh_CN.utf-8
endif

" AUTO COMMANDS:
" auto expand tab to blanks
"autocmd FileType c,cpp set expandtab
if has("autocmd")
	"autocmd FileType * set tabstop=4|set shiftwidth=4|set softtabstop=4|set noexpandtab
	"autocmd FileType python set tabstop=4|set shiftwidth=4|set expandtab
	autocmd FileType c,cpp set noexpandtab
	autocmd FileType rc set expandtab

	" vimrc_on_the_fly
	"autocmd bufwritepost .vimrc source ~/.vimrc

	" Restore the last quit position when open file.
	autocmd BufReadPost *
				\ if line("'\"") > 0 && line("'\"") <= line("$") |
				\     exe "normal g'\"" |
				\ endif
endif

" SHORTCUT SETTINGS:
" Set mapleader
let mapleader=","
" Space to command mode.
nnoremap <space> :
vnoremap <space> :
" Switching between buffers.
nnoremap <C-h> <C-W>h
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-l> <C-W>l
inoremap <C-h> <Esc><C-W>h
inoremap <C-j> <Esc><C-W>j
inoremap <C-k> <Esc><C-W>k
inoremap <C-l> <Esc><C-W>l

map <C-h> <C-W>h
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-l> <C-W>l

" "cd" to change to open directory.
let OpenDir=system("pwd")
nmap <silent> <leader>cdr :exe 'cd ' . OpenDir<cr>:pwd<cr>
nmap <silent> <leader>cdf :cd %:h<cr>:pwd<cr>

" PLUGIN SETTINGS:
" taglist.vim
let g:Tlist_Auto_Update=1
let g:Tlist_Process_File_Always=1
let g:Tlist_Exit_OnlyWindow=1
let g:Tlist_Show_One_File=1
let g:Tlist_WinWidth=25
let g:Tlist_Enable_Fold_Column=0
let g:Tlist_Auto_Highlight_Tag=1
let g:Tlist_Auto_Open=0
let g:Tlist_Use_Right_Window=0
let g:Tlist_File_Fold_Auto_Close = 1
" NERDTree.vim
let g:NERDTreeWinPos="right"
let g:NERDTreeWinSize=25
let g:NERDTreeShowLineNumbers=1
let g:NERDTreeQuitOnOpen=1
" cscope.vim
if has("cscope")
    " use both cscope and ctag for 'ctrl-]', ':ta', and 'vim -t'
	set cscopetag
	"显示tags所匹配到的所有
	set cst
    set csto=1
    set nocsverb
    if filereadable("cscope.out")
        cs add cscope.out
    endif
    set csverb
endif
function! LoadCscope()
	let db = findfile("cscope.out", ".;")
	if (!empty(db))
		let path = strpart(db, 0, match(db, "/cscope.out$"))
		set nocscopeverbose " suppress 'duplicate connection' error
		exe "cs add " . db . " " . path
		set cscopeverbose
	endif
endfunction
au BufEnter /* call LoadCscope()
" OmniCppComplete.vim
let g:OmniCpp_DefaultNamespaces=["std"]
let g:OmniCpp_MayCompleteScope=1
let g:OmniCpp_SelectFirstItem=2
" VimGDB.vim
if has("gdb")
	set asm=0
	let g:vimgdb_debug_file=""
	run macros/gdb_mappings.vim
endif
" LookupFile setting
let g:LookupFile_TagExpr='"tags.fn"'
let g:LookupFile_MinPatLength=2
let g:LookupFile_PreserveLastPattern=0
let g:LookupFile_PreservePatternHistory=1
let g:LookupFile_AlwaysAcceptFirst=1
let g:LookupFile_AllowNewFiles=0
" Man.vim
source $VIMRUNTIME/ftplugin/man.vim
" snipMate
let g:snips_author="ZhangXiang"
let g:snips_email="qi.mu.zhi@163.com"
let g:snips_copyright="xxxxxxxx, Inc"
" plugin shortcuts
function! RunShell(Msg, Shell)
	echo a:Msg . '...'
	call system(a:Shell)
	echon 'done'
endfunction
nmap  <F2> :TlistToggle<cr>
nmap  <F3> :NERDTreeToggle<cr>
nmap  <F4> :MRU<cr>
nmap  <F5> viBzf
nmap  <F6> zd
nmap  <F7> :vimgrep <C-R>=expand("<cword>")<cr> **/*.c **/*.h **/*.S **/*.s<cr><C-o>:cw<cr>
nmap  <F9> :call RunShell("Generate tags", "ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .")<cr>
nmap <F10> :call HLUDSync()<cr>
"nmap <F11> :call RunShell("Generate filename tags", "~/.vim/shell/genfiletags.sh")<cr>
nmap <F12> :call RunShell("Generate cscope", "cscope -Rb")<cr>:cs add cscope.out<cr>
nmap <leader>sa :cs add cscope.out<cr>
nmap <leader>ss :cs find s <C-R>=expand("<cword>")<cr><cr>
nmap <leader>sg :cs find g <C-R>=expand("<cword>")<cr><cr>
nmap <leader>sc :cs find c <C-R>=expand("<cword>")<cr><cr>
nmap <leader>st :cs find t <C-R>=expand("<cword>")<cr><cr>
nmap <leader>se :cs find e <C-R>=expand("<cword>")<cr><cr>
nmap <leader>sf :cs find f <C-R>=expand("<cfile>")<cr><cr>
nmap <leader>si :cs find i <C-R>=expand("<cfile>")<cr><cr>
nmap <leader>sd :cs find d <C-R>=expand("<cword>")<cr><cr>
nmap <leader>zz <C-w>o
nmap <leader>gs :GetScripts<cr>

"nnoremap <C-TAB> :tabnext<CR>
"nnoremap <C-S-TAB> :tabprev<CR>
"nnoremap <C-T> :tabnew . <CR>
"nmap <leader>tn	:tabnew . <cr>
"nnoremap <leader>n	:tabnext<cr>

let g:winManagerWindowLayout='FileExplorer|BufExplorer'
let g:persistentBehaviour=0
let g:winManagerWidth=20
let g:defaultExplorer=1
nmap <silent> <leader>fir :FirstExplorerWindow<cr>
nmap <silent> <leader>bot :BottomExplorerWindow<cr>
nmap <silent> <leader>wm :WMToggle<cr>

" Indent Guides is a plugin for visually displaying indent levels in Vim.
" USAGE:
" The default mapping to toggle the plugin is `<Leader>ig`
"let g:indent_guides_guide_size = 1
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=3
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=4


"Set the "tab" as "|"
"set list lcs=tab:\|\ (There is a space!!)
"set listchars=tab:▸\ ,eol:¬,trail:¬
set listchars=tab:▸\ ,trail:█,extends:¬
"set listchars=tab:\▸\ ,nbsp:%,trail:-
"highlight WhitespaceEOL ctermbg=red guibg=red
"match WhitespaceEOL /\s\+$/
set list

"Set tags
set tags=tags;

"mimiBufExpl
let g:miniBufExplMapWindowNavVim = 1
let g:miniBufExplMapWindowNavArrows = 1
let g:miniBufExplMapCTabSwitchBufs = 1
let g:miniBufExplModSelTarget = 1

"set clipboard=unnamed
"set clipboard=unnamedplus

nmap <silent> <leader>p :set paste<CR>"*p:set nopaste<CR>
vmap <c-c> "+y
"vmap <c-c> "+y
"map  <c-v> "+p
"map! <C-v> <C-c>"+p

nmap <C-a> :w !sudo tee %<CR><CR><ESC><ESC>:e!<CR>
vmap <C-a> <C-C>:w !sudo tee %<CR><CR><ESC><ESC>:e!<CR>
imap <C-a> <C-O>:w !sudo tee %<CR><CR><ESC><ESC>:e!<CR>

nmap <C-s> :w!<CR>
vmap <C-s> <C-C>:w!<CR>
imap <C-s> <C-O>:w!<CR>

map <C-q> :q!<CR>
vmap <C-q> <C-C>:q!<CR>
imap <C-q> <C-O>:q!<CR>

"nmap <C-q> :qa!<CR>
"vmap <C-q> <C-C>:qa!<CR>
"imap <C-q> <C-O>:qa!<CR>

nmap <C-x> :wqa!<CR>
vmap <C-x> <C-C>:wqa!<CR>
imap <C-x> <C-O>:wqa!<CR>

" Bubble single lines
"nmap <C-Up> ddkP
"nmap <C-Down> ddp
nmap <C-Up> [e
nmap <C-Down> ]e
" Bubble multiple lines
"vmap <C-Up> xkP`[V`]
"vmap <C-Down> xp`[V`]
vmap <C-Up> [egv
vmap <C-Down> ]egv

let g:pydiction_location = '~/.vim/after/ftplugin/pydiction/complete-dict'
let g:pydiction_menu_height = 20

""echofunc plugin
"let g:EchoFuncKeyNext='<S-n>'
"let g:EchoFuncKeyPrev='<S-p>'
let g:EchoFuncKeyNext='<leader>fn'
let g:EchoFuncKeyPrev='<leader>fp'
let g:EchoFuncAutoStartBalloonDeclaration = 1

