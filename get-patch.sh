#!/bin/bash

#使用方法：在对应的git库下面执行：get-patch.sh FILENAME COMMIT_ID

if [ "$1" = "-h" ]; then
	echo "使用方法：在对应的git库下面执行：get-patch.sh FILENAME COMMIT_ID"
	echo "生成路径：$HOME/dir/Documents/bypy/design-patch/"
else
workdir=$(pwd)
pathdir=${workdir##*rom}
echo $pathdir

BEFOREDIR=$HOME/dir/Documents/bypy/design-patch/$1/original/$pathdir/$3
AFTERDIR=$HOME/dir/Documents/bypy/design-patch/$1/modify/$pathdir/$3

GITHEAD=$2
mkdir -p $BEFOREDIR
mkdir -p $AFTERDIR

if [ "x$2" = "x" ]; then
	GITHEAD="HEAD"
fi


for f in `git diff $GITHEAD~1..$GITHEAD --name-only`; do
	echo "copy... $f"
	rm -fr $f
	git checkout $GITHEAD~1 $f
	cp --parents $f $BEFOREDIR
	git checkout $GITHEAD $f
	cp --parents $f $AFTERDIR
done

fi

