#!/bin/bash

#find -name "*${1}*" |grep ${1} --color=auto
#find . -path '*/.git*' -prune -o -print
#find . -name .git -a -type d -prune -o -print
#find . -not -iwholename '*.git*' -name "*${1}*" | grep ${1} --color=auto
#sudo find -not -path "./.git/*" -not -path "./.svn/*" -not -path \
	#"./.repo/*"  -iname "*${1}*" | grep "${1}" --color=auto -i
#sudo find -not -path "*/.git/*" -not -path "*/.svn/*" -not -path \
	#"*/.repo/*"  -iname "*${1}*" | xargs -i echo -e "\e[41m{}\e[0m"
sudo find -not -path "*/.git/*" -not -path "*/.svn/*" -not -path \
	"*/.repo/*"  -iname "*${1}*" | grep -iP "\Q${1}\E" --color=auto

