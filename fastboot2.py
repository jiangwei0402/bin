#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
2015-10-22 17:50
Li Guofeng<ligf0702@thundersoft.com>
"""
'''fastboot.py:

    Description:
    
    fastboot.py flashes all the binaries that bases on partition.xml and contents.xml.    
    
Usage:

    python fastboot.py romimage-path 
    
'''
import os
import sys
import subprocess
from optparse import OptionParser

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
    
#CONTENTS="contents.xml"
#PARTITION="partition.xml"
CONTENTS=""
PARTITION=""

#imageDict = {./} ##image name and path dictionary
#patitionDict = {} ##image name and patition name dictionary

def PrintError(errorstr):
    print '\033[1;31m',
    print errorstr
    print '\033[0m',
                
def ParseContents(contents):
    imageDict = {./}
    
    if cmp(contents, "") is 0:
        return imageDict
        
    contentsTree = ET.parse(contents)
    ##Search the element of build
    buildNodes = contentsTree.iter('build')
    PrintError('*' * 50)
    
    for i in buildNodes:        
        for j in i.iter("name"):
            print j.text
            
        for j in i.iter('download_file'): ##Search the element of download_file
            for n in j.iter('file_name'): ##print file_name
                print '     ',
                print("%-30s"%n.text),
                for m in j.iter('file_path'): ##print file_path
                    if cmp(m.get("flavor"), "modemlite") is 0:
                        continue
                    print '---',
                    print("%s"%(m.text))
                    ##Judge the path of repetition
                    if imageDict.has_key(n.text) is False:
                        imageDict[n.text] = m.text
                    elif cmp(imageDict.get(n.text), m.text) is 0:
                        None
                    elif cmp(imageDict.get(n.text), "*") is 0:
                        None
                    else :
                        PrintError("ERROR: Image "+n.text+" has different path:")
                        PrintError(m.text)
                        PrintError(imageDict.get(n.text))
    
        for j in i.iter('file_ref'): ##Search the element of file_ref
            for n in j.iter('file_name'): ##print file_name
                print '     ',
                print("%-30s"%n.text),
                for m in j.iter('file_path'): ##print file_path
                    if cmp(m.get("flavor"), "modemlite") is 0:
                        continue
                    print '---',
                    print("%s"%(m.text))
                    ##Judge the path of repetition
                    if imageDict.has_key(n.text) is False:
                        imageDict[n.text] = m.text
                    elif cmp(imageDict.get(n.text), m.text) is 0:
                        None
                    elif cmp(n.text, "*") is 0:
                        None
                    else :
                        PrintError("WARNING: Image "+n.text+" has different path:")
                        PrintError(m.text)
                        PrintError(imageDict.get(n.text))

    PrintError('*' * 50)
    return imageDict
    
def ParsePartition(partition):
    patitionDict = {}
    patitionTree = ET.parse(partition)
    ##Search the element of physical_partition
    partitionNodes = patitionTree.iter('partition')
    
    for i in partitionNodes:
        if cmp(i.get("filename"), "") is 0:
            continue
        if i.get("filename") is None:
            continue
        print("%-13s"%i.get("label")),
        print "---",
        print i.get("filename")
        patitionDict[i.get("label")] = i.get("filename")
    PrintError('*' * 50)
    return patitionDict

#Build Fastboot Meta and Make sure that the file is there.
def BuildFastbootMeta(contentsMeta, partitionMeta):
    #{('boot.img', 'LINUX/android/out/debug/target/product/xxx/')}
    imageDict = ParseContents(contentsMeta)
    
    #{('tzbak', 'tz.mbn')}
    partitionDict = ParsePartition(partitionMeta)
    
    #data of fastboot
    fastbootMetaDict = {}
    
    #common/build/emmc/
    #LINUX/android/out/debug/target/product/xxx/
    defaultImagePathList = [".", imageDict.get("gpt_both0.bin"), imageDict.get("boot.img") ]

    ##Make sure gpt_both0.bin is there
    if imageDict.get("gpt_both0.bin") is None:
        PrintError("ERRRR Don't find the path of gpt_both0.bin at contents.xml")
        sys.exit()
    else:
        tmp=os.path.abspath(imageDict.get("gpt_both0.bin")+"/../../../"+"/../../../gpt_both0.bin")
        if os.path.isfile(tmp) is True:
            fastbootMetaDict["partition"] = tmp
        else:
            PrintError("ERRRR Don't find :")
            print "gpt_both0.bin"+"   at   "+tmp
            PrintError('*' * 50)
            sys.exit()
    
    for i in partitionDict.items():
        found = False
        if imageDict.get(i[1]) is None:
            #Search the image at the default path
            for j in defaultImagePathList:
                tmp=os.path.abspath(j+"/"+i[1])
                if os.path.isfile(tmp) is True:
                    found = True
                    fastbootMetaDict[i[0]] = tmp
                    break
            if found is False:
                PrintError("ERRRR Don't find :")
                PrintError("%-25s"%i[1])
                sys.exit()
        #Found the image path in partition.xml            
        else:
            tmp=os.path.abspath(imageDict.get(i[1])+"/"+i[1])
            if os.path.isfile(tmp) is True:
                fastbootMetaDict[i[0]] = tmp
            else:
                PrintError("ERRRR Don't find :")
                print i[1]+"   at   "+tmp
                PrintError('*' * 50)
                sys.exit()
    return fastbootMetaDict

def CheckFastbootDevices(fastboot) :
    #check if fastboot is available.
    try:
        stuff = subprocess.Popen([fastboot, 'devices'], stdout=subprocess.PIPE).stdout.read()
    except:
        print "\nFastboot is not installed."
        print "Please install fastboot."
        sys.exit(0)
    #check devices if it's in fastboot mode      
    if stuff.count('fastboot') >= 1:
        return
    else:
        print  "Please check if USB is connected."
        print  "If USB is connected, Power cycle the device to fastboot."
        sys.exit(0)

def FastbootFlash(partition, image):
   fastboot_command = ['fastboot', 'flash', partition, image]
   print ' '.join(fastboot_command)
   stuff = subprocess.Popen(fastboot_command, stdout=subprocess.PIPE,stderr=subprocess.STDOUT).stdout.read()
   print stuff
   if stuff.count('OKAY') < 2:
      print "Failed to flash images"
      print "Exiting"
      sys.exit("Exiting")
   return

def FindContentsXML(path):
    global CONTENTS
    tmp=os.path.abspath(path+"/contents.xml")
    if os.path.isfile(tmp) is True:
        CONTENTS=tmp
    else:
        print "Can't find contents.xml at the directory!"
        sys.exit("Exiting")
    
    print os.path.abspath(CONTENTS)

def FindPartitionXML(path):
    global PARTITION
    tmp=os.path.abspath(path+"/partition.xml")
    if os.path.isfile(tmp) is True:
        PARTITION=tmp
    else:
        tmp=os.path.abspath(path+"/partition.xml")
        if os.path.isfile(tmp) is True:
            PARTITION=tmp
        else:
            print "Can't find partition.xml at the directory!"
            sys.exit("Exiting")
    print os.path.abspath(PARTITION)

##main()
parser = OptionParser()
(options, args) = parser.parse_args()

PrintError("You are at "+os.path.abspath(os.curdir))
if len(args) == 0:
    PrintError("Default: Fonud the XML at current directory.")
elif len(args) == 1: #Change current path to Rom directory.
    os.chdir(args[0])
else:
    PrintError("Usage:\n     python fastboot.py PATH")
    sys.exit("Args Error.")

FindContentsXML(".")
FindPartitionXML(".")

CheckFastbootDevices("fastboot")
fastbootMeta = BuildFastbootMeta(CONTENTS, PARTITION)
fastbootMeteList = fastbootMeta.items()
fastbootMeteList.sort()
FastbootFlash("partition", fastbootMeta.get("partition"))
for i in fastbootMeteList:
    print i
    if cmp(i[0], "partition") is 0:
        continue
    FastbootFlash(i[0], i[1])
